﻿#SingleInstance Force
#IfWinActive GTA:SA:MP
#Include bin/puReAPI.ahk
#Include bin/getGameText.ahk
#Include bin/statsAPI.ahk
#UseHook
#NoEnv

/*

///////////////////////////////////########################///////////////////////////////////
///////////////////////////////////#      by puRe         #///////////////////////////////////
///////////////////////////////////# https://d0me.xyz     #///////////////////////////////////
///////////////////////////////////# https://headzfetz.pw #///////////////////////////////////
///////////////////////////////////########################///////////////////////////////////

*/

SetTimer, Timer1, 100
SetTimer, DrogenTimer, off
SetTimer, FischenTimer, off

global myName 				:= "Unknown"
global ChatMsgColor			:= "FF0000"
global DLLLoaded			:= 0
global bFischen				:= false

global currentKills			:= 0

global dailyKills 			:= 0
global dailyDeaths 			:= 0
global dailyDrugs 			:= 0
global dailyZinsen 			:= 0
global dailyPayday 			:= 0
global dailyPrison 			:= 0
global dailyWaffen			:= 0

global monthlyKills 		:= 0
global monthlyDeaths		:= 0
global monthlyDrugs 		:= 0
global monthlyZinsen 		:= 0
global monthlyPayday 		:= 0
global monthlyPrison 		:= 0
global monthlyWaffen		:= 0

global globalKills	 		:= 0
global globalDeaths 		:= 0
global globalDrugs 			:= 0
global globalZinsen 		:= 0
global globalPayday 		:= 0
global globalPrison 		:= 0
global globalWaffen			:= 0

global Wanted_Level 		:= 0
global Wanted_Grund 		:= "Unknown"
global Wanted_Reporter 		:= "Unknown"
global Wanted_Time 			:= "00:00:00"

global DrogenDabei			:= 0
global DrogenGenommen		:= 0
global LastNumber			:= 0

global Today := ""
global Month := ""

FormatTime,Today,,dd.MM.yyyy
FormatTime,Month,,MMMM yyyy

#Include create_and_read_settings.ahk

InitKeys()
InitCKeys()

Gui, Add, Picture, x0 y0 w1095 h725, %A_WorkingDir%\Pics\bg3.jpg
Gui, Add, GroupBox, x45 y75 w1000 h250 cWhite +BackgroundTrans,
Gui,Font,s10,Verdana
Gui, Add, Text, x340 y90 +BackgroundTrans cWhite, Tagesstatistik (%Today%)
Gui, Add, Text, x565 y90 +BackgroundTrans cWhite, Monatsstatistik (%Month%)
Gui, Add, Text, x780 y90 +BackgroundTrans cWhite, Gesamtstatistik (Seit Erststart)

Gui, Add, Text, x65 y125 +BackgroundTrans cWhite, Deine Kills:
Gui, Add, Text, x65 y150 +BackgroundTrans cWhite, Deine Tode:
Gui, Add, Text, x65 y175 +BackgroundTrans cWhite, Drogen benutzt:
Gui, Add, Text, x65 y200 +BackgroundTrans cWhite, Zinsen erhalten:
Gui, Add, Text, x65 y225 +BackgroundTrans cWhite, Zeit im Prison:
Gui, Add, Text, x65 y250 +BackgroundTrans cWhite, Paydays erhalten:
Gui, Add, Text, x65 y275 +BackgroundTrans cWhite, Geld für Waffen:

Gui, Add, Text, x340 y125 +BackgroundTrans cWhite, % number_format(dailyKills) " Kills"
Gui, Add, Text, x340 y150 +BackgroundTrans cWhite, % number_format(dailyDeaths) " Tode"
Gui, Add, Text, x340 y175 +BackgroundTrans cWhite, % number_format(dailyDrugs) " Gramm"
Gui, Add, Text, x340 y200 +BackgroundTrans cWhite, % number_format(dailyZinsen) " $"
Gui, Add, Text, x340 y225 +BackgroundTrans cWhite, % number_format(dailyPrison) " sek"
Gui, Add, Text, x340 y250 +BackgroundTrans cWhite, % number_format(dailyPayday) " stk"
Gui, Add, Text, x340 y275 +BackgroundTrans cWhite, % number_format(dailyWaffen) " $"

Gui, Add, Text, x565 y125 +BackgroundTrans cWhite, % number_format(monthlyKills) " Kills"
Gui, Add, Text, x565 y150 +BackgroundTrans cWhite, % number_format(monthlyDeaths) " Tode"
Gui, Add, Text, x565 y175 +BackgroundTrans cWhite, % number_format(monthlyDrugs) " Gramm"
Gui, Add, Text, x565 y200 +BackgroundTrans cWhite, % number_format(monthlyZinsen) " $"
Gui, Add, Text, x565 y225 +BackgroundTrans cWhite, % number_format(monthlyPrison) " sek"
Gui, Add, Text, x565 y250 +BackgroundTrans cWhite, % number_format(monthlyPayday) " stk"
Gui, Add, Text, x565 y275 +BackgroundTrans cWhite, % number_format(monthlyWaffen) " $"

Gui, Add, Text, x780 y125 +BackgroundTrans cWhite, % number_format(globalKills) " Kills"
Gui, Add, Text, x780 y150 +BackgroundTrans cWhite, % number_format(globalDeaths) " Tode"
Gui, Add, Text, x780 y175 +BackgroundTrans cWhite, % number_format(globalDrugs) " Gramm"
Gui, Add, Text, x780 y200 +BackgroundTrans cWhite, % number_format(globalZinsen) " $"
Gui, Add, Text, x780 y225 +BackgroundTrans cWhite, % number_format(globalPrison) " sek"
Gui, Add, Text, x780 y250 +BackgroundTrans cWhite, % number_format(globalPayday) " stk"
Gui, Add, Text, x780 y275 +BackgroundTrans cWhite, % number_format(globalWaffen) " $"

Gui, Add, Button, x285 y15 w150 h40 gEinstellungen, Einstellungen
Gui, Add, Button, x465 y15 w150 h40 gHotkeys, Hotkeys
Gui, Add, Button, x645 y15 w150 h40 gTextbinds, Textbinds
Gui, Add, Button, x845 y15 w150 h40 gCustomKeys, Eigene Binds
;~ Gui, Show, w1095 h725, Keybinder by DOME

Gui 2: Add, Picture, x0 y0 w1095 h725, %A_WorkingDir%\Pics\bg3.jpg
Gui 2: Add, Text, x5 y5 +BackgroundTrans cWhite, /gtake drugs _ ?
Gui 2: Add, Text, x5 y30 +BackgroundTrans cWhite, /buygun deagle _ ?
Gui 2: Add, Text, x5 y55 +BackgroundTrans cwhite, /buygun shotgun _ ?
Gui 2: Add, Text, x5 y80 +BackgroundTrans cWhite, GangWarKillSpruch
Gui 2: Add, Text, x5 y105 +BackgroundTrans cWhite, GangZoneKillSpruch
Gui 2: Add, Text, x5 y130 +BackgroundTrans cWhite, Chat Nachricht ;ChatNachricht
Gui 2: Add, Text, x455 y5 +BackgroundTrans cWhite, Waffenpack

Gui 2: Add, Edit, x155 y5 w100 +BackgroundTrans vDrugsMitnehmen, %DrugsMitnehmen%
Gui 2: Add, Edit, x155 y30 w100 +BackgroundTrans vDeagleKaufen, %DeagleKaufen%
Gui 2: Add, Edit, x155 y55 w100 +BackgroundTrans vShotgunKaufen, %ShotgunKaufen%
Gui 2: Add, Edit, x155 y80 w275 +BackgroundTrans vSpruch1, %KillSpruch%
Gui 2: Add, Edit, x155 y105 w275 +BackgroundTrans vSpruch2, %GangZoneSpruch%
Gui 2: Add, Edit, x155 y130 w275 +BackgroundTrans vChatNachricht, %ChatNachricht%
Gui 2: Add, DropDownList, AltSubmit Choose%WeaponPack1% vWeaponPack1 x455 y35 +BackgroundTrans, Deagle|Shotgun|MP5|M4|Sniper|Rifle
Gui 2: Add, DropDownList, AltSubmit Choose%WeaponPack2% vWeaponPack2 x455 y60 +BackgroundTrans, Deagle|Shotgun|MP5|M4|Sniper|Rifle
Gui 2: Add, DropDownList, AltSubmit Choose%WeaponPack3% vWeaponPack3 x455 y85 +BackgroundTrans, Deagle|Shotgun|MP5|M4|Sniper|Rifle
Gui 2: Add, DropDownList, AltSubmit Choose%WeaponPack4% vWeaponPack4 x455 y110 +BackgroundTrans, Deagle|Shotgun|MP5|M4|Sniper|Rifle
Gui 2: Add, Edit, x585 y35 w100 +BackgroundTrans vWeaponPackAmmo1, %WeaponPackAmmo1%
Gui 2: Add, Edit, x585 y60 w100 +BackgroundTrans vWeaponPackAmmo2, %WeaponPackAmmo2%
Gui 2: Add, Edit, x585 y85 w100 +BackgroundTrans vWeaponPackAmmo3, %WeaponPackAmmo3%
Gui 2: Add, Edit, x585 y110 w100 +BackgroundTrans vWeaponPackAmmo4, %WeaponPackAmmo4%

Gui 2: Add, Text, x700 y85 +BackgroundTrans cWhite, Sprüche:

Gui 2: Add, Checkbox, x750 y35 w275 +BackgroundTrans vKillStreaks Checked%KillStreaks%, KillStreaks?
Gui 2: Add, Checkbox, x750 y60 w275 +BackgroundTrans vKillSpruche Checked%KillSpruche%, KillSprüche?
Gui 2: Add, Edit, x750 y85 w275  +BackgroundTrans vKillSpruche1, %KillSpruche1%
Gui 2: Add, Edit, x750 y110 w275 +BackgroundTrans vKillSpruche2, %KillSpruche2%
Gui 2: Add, Edit, x750 y135 w275 +BackgroundTrans vKillSpruche3, %KillSpruche3%
Gui 2: Add, Edit, x750 y160 w275 +BackgroundTrans vKillSpruche4, %KillSpruche4%
Gui 2: Add, Edit, x750 y185 w275 +BackgroundTrans vKillSpruche5, %KillSpruche5%


Gui 3: Add, Picture, x0 y0 w1095 h725, %A_WorkingDir%\Pics\bg3.jpg
;~ Gui 3: Add, Text, x5 y5 w180 +BackgroundTrans, Hotkeys

Gui 3: Add, Text, x5 y5 +BackgroundTrans cWhite, /members ;1 Member
Gui 3: Add, Text, x5 y30 +BackgroundTrans cWhite, /finances ;2 Finances
Gui 3: Add, Text, x5 y55 +BackgroundTrans cWhite, /gebietinfo ;3 GEbietinfo
Gui 3: Add, Text, x5 y80 +BackgroundTrans cWhite, /engine und /lock ;4 Motor und /lock
Gui 3: Add, Text, x5 y105 +BackgroundTrans cWhite, /lock ;5 /lock
Gui 3: Add, Text, x5 y130 +BackgroundTrans cWhite, /usedrugs ;6 /usedrugs
Gui 3: Add, Text, x5 y155 +BackgroundTrans cWhite, /time ;7 /time
Gui 3: Add, Text, x5 y180 +BackgroundTrans cWhite, /enter o. /exit ;8 /enter
Gui 3: Add, Text, x5 y205 +BackgroundTrans cWhite, HP ;9 hp
Gui 3: Add, Text, x5 y230 +BackgroundTrans cWhite, /lights ;10 /lights
Gui 3: Add, Text, x5 y255 +BackgroundTrans cWhite, /kidnap 0 ;11 kidnap 0
Gui 3: Add, Text, x5 y280 +BackgroundTrans cWhite, /kidnap 1 ;12 kidnap 1
Gui 3: Add, Text, x5 y305 +BackgroundTrans cWhite, Drogenzähler ;13 drogenzähler
Gui 3: Add, Text, x5 y330 +BackgroundTrans cWhite, Vielen Dank ;14 danke

Gui 3: Add, Text, x255 y5 +BackgroundTrans cWhite, Heal bitte ;15 heal pls
Gui 3: Add, Text, x255 y30 +BackgroundTrans cWhite, /f HP ;16 /f hp
Gui 3: Add, Text, x255 y55 +BackgroundTrans cWhite, Schönen Tag noch ;17 schönen tag
Gui 3: Add, Text, x255 y80 +BackgroundTrans cWhite, /Stats ;18 stats
Gui 3: Add, Text, x255 y105 +BackgroundTrans cWhite, /deagle %DeagleKaufen% ;19 /buygun deagle 150
Gui 3: Add, Text, x255 y130 +BackgroundTrans cWhite, /shotgun %ShotgunKaufen% ;20 /buygun shotgun 100
Gui 3: Add, Text, x255 y155 +BackgroundTrans cWhite, Waffenpack ;21 m4 deagle shot
Gui 3: Add, Text, x255 y180 +BackgroundTrans cWhite, /carlock ;22 /carlock
Gui 3: Add, Text, x255 y205 +BackgroundTrans cWhite, /fill /get fuel ;23 /fill und /get fuel
Gui 3: Add, Text, x255 y230 +BackgroundTrans cWhite, Wanteds im /f ;24 wanteds f chat
Gui 3: Add, Text, x255 y255 +BackgroundTrans cWhite, /togphone ;25 togphone
Gui 3: Add, Text, x255 y280 +BackgroundTrans cWhite, /eatpizza ;26 eatpizza
Gui 3: Add, Text, x255 y305 +BackgroundTrans cWhite, Letzte Eingabe wdh. ;27 wdhholen
Gui 3: Add, Text, x255 y330 +BackgroundTrans cWhite, /gtake drugs %DrugsMitnehmen% ;28 take drugs

Gui 3: Add, Text, x505 y5 +BackgroundTrans cWhite, GangWarKill ;29 gw kill
Gui 3: Add, Text, x505 y30 +BackgroundTrans cWhite, /engine ;30 /engine
Gui 3: Add, Text, x505 y55 +BackgroundTrans cWhite, /notafk ;31 /notafk

Gui 3: Add, Hotkey, vKey1 x95 y5 +BackgroundTrans, %Key1%
Gui 3: Add, Hotkey, vKey2 x95 y30 +BackgroundTrans, %Key2%
Gui 3: Add, Hotkey, vKey3 x95 y55 +BackgroundTrans, %Key3%
Gui 3: Add, Hotkey, vKey4 x95 y80 +BackgroundTrans, %Key4%
Gui 3: Add, Hotkey, vKey5 x95 y105 +BackgroundTrans, %Key5%
Gui 3: Add, Hotkey, vKey6 x95 y130 +BackgroundTrans, %Key6%
Gui 3: Add, Hotkey, vKey7 x95 y155 +BackgroundTrans, %Key7%
Gui 3: Add, Hotkey, vKey8 x95 y180 +BackgroundTrans, %Key8%
Gui 3: Add, Hotkey, vKey9 x95 y205 +BackgroundTrans, %Key9%
Gui 3: Add, Hotkey, vKey10 x95 y230 +BackgroundTrans, %Key10%
Gui 3: Add, Hotkey, vKey11 x95 y255 +BackgroundTrans, %Key11%
Gui 3: Add, Hotkey, vKey12 x95 y280 +BackgroundTrans, %Key12%
Gui 3: Add, Hotkey, vKey13 x95 y305 +BackgroundTrans, %Key13%
Gui 3: Add, Hotkey, vKey14 x95 y330 +BackgroundTrans, %Key14%

Gui 3: Add, Hotkey, vKey15 x345 y5 +BackgroundTrans, %Key15%
Gui 3: Add, Hotkey, vKey16 x345 y30 +BackgroundTrans, %Key16%
Gui 3: Add, Hotkey, vKey17 x345 y55 +BackgroundTrans, %Key17%
Gui 3: Add, Hotkey, vKey18 x345 y80 +BackgroundTrans, %Key18%
Gui 3: Add, Hotkey, vKey19 x345 y105 +BackgroundTrans, %Key19%
Gui 3: Add, Hotkey, vKey20 x345 y130 +BackgroundTrans, %Key20%
Gui 3: Add, Hotkey, vKey21 x345 y155 +BackgroundTrans, %Key21%
Gui 3: Add, Hotkey, vKey22 x345 y180 +BackgroundTrans, %Key22%
Gui 3: Add, Hotkey, vKey23 x345 y205 +BackgroundTrans, %Key23%
Gui 3: Add, Hotkey, vKey24 x345 y230 +BackgroundTrans, %Key24%
Gui 3: Add, Hotkey, vKey25 x345 y255 +BackgroundTrans, %Key25%
Gui 3: Add, Hotkey, vKey26 x345 y280 +BackgroundTrans, %Key26%
Gui 3: Add, Hotkey, vKey27 x345 y305 +BackgroundTrans, %Key27%
Gui 3: Add, Hotkey, vKey28 x345 y330 +BackgroundTrans, %Key28%

Gui 3: Add, Hotkey, vKey29 x595 y5 +BackgroundTrans, %Key29%
Gui 3: Add, Hotkey, vKey30 x595 y30 +BackgroundTrans, %Key30%
Gui 3: Add, Hotkey, vKey31 x595 y55 +BackgroundTrans, %Key31%

Gui 4: Add, Picture, x0 y0 w1195 h725, %A_WorkingDir%\Pics\bg3.jpg
Textbinds1=
(
/deagle => /buygun deagle`n
/shotgun => /buygun shotgun`n
/mp5 => /buyguyn mp5`n
/m4 => /buygun m4`n
/sniper => /buygun sniper`n
/pa => Partner`n
/ku => Opfer`n
/napinfo => Napinfo`n
/setkills => Kills setzen`n
/settode => Tode setzen`n
/addkill => Kill addieren`n
/addtod => Tod addieren`n
/clearkill => Kill löschen`n
/cleartod => Tod löschen`n
)
Gui 4: Add, Text, x5 y5 +BackgroundTrans cWhite, %Textbinds1%
Textbinds2=
(
/dd => Drogenzähler zurücksetzen`n
/fdkd => DKD im /f`n
/bdkd => DKD im /b`n
/dkd => DKD`n
/gdkd => DKD im /g`n
/kd => KD`n
/bkd => KD im /b`n
/fkd => KD im /f`n
/gkd => KD im /g`n
/bmkd => MKD im /b`n
/mkd => MKD`n
/fmkd => MKD im /f`n
/gmkd => MKD im /g`n
/25k => 25k auszahlen`n
)
Gui 4: Add, Text, x255 y5 +BackgroundTrans cWhite, %Textbinds2%
Textbinds3=
(
/99k => 99k auszahlen`n
/50k => 50k auszahlen`n
/75k => 75k auszahlen`n
/150k => 150k auszahlen`n
/200k => 200k auszahlen`n
/300k => 300k auszahlen`n
/500k => 500k auszahlen`n
/999k => 999k auszahlen`n
/zinsen => Zinsinformationen`n
/kstand => finances im /b`n
/fstand => finances im /f`n
/gstand => finances im /g`n
/mms, /idsms => /number -> /sms`n
/re => letzte SMS antworten`n
)
Gui 4: Add, Text, x505 y5 +BackgroundTrans cWhite, %Textbinds3%
Textbinds4=
(
/wi => /warinfo`n
/td => /gtake drugs`n
/pd => /put drugs`n
/cd => Countdown`n
/as => Auto Accept`n
/wh => Wheelman rufen`n
/ff => Hotdog anbieten`n
/gr => /giverank`n
/setdrugs => Drogen setzen`n
/ud => /usedrugs`n
/setlbs => MinLBS Größe`n
/fischen => Fischerschleife`n
/stopfischen => Fischen stoppen`n
/drugprice => Drogenpreis berechnen`n
)
Gui 4: Add, Text, x755 y5 +BackgroundTrans cWhite, %Textbinds4%
Textbinds5=
(
/mydrugs => Meine Drogenwert`n
/bmydrugs => Drogenwert im Chat`n
/fmydrugs => Drogenwert im f-Chat`n
/gmydrugs => Drogenwert im g-Chat`n
/gi => Gebietinfo`n
/100k => 100k abheben`n
/rifle => /buygun rifle`n
)
Gui 4: Add, Text, x1005 y5 +BackgroundTrans cWhite, %Textbinds5%


Gui 5: Add, Picture, x0 y0 w1095 h725, %A_WorkingDir%\Pics\bg3.jpg
Gui 5: Add, Hotkey, vCKey1 x95 y5 +BackgroundTrans, %CKey1%
Gui 5: Add, Hotkey, vCKey2 x95 y30 +BackgroundTrans, %CKey2%
Gui 5: Add, Hotkey, vCKey3 x95 y55 +BackgroundTrans, %CKey3%
Gui 5: Add, Hotkey, vCKey4 x95 y80 +BackgroundTrans, %CKey4%
Gui 5: Add, Hotkey, vCKey5 x95 y105 +BackgroundTrans, %CKey5%
Gui 5: Add, Hotkey, vCKey6 x95 y130 +BackgroundTrans, %CKey6%
Gui 5: Add, Hotkey, vCKey7 x95 y155 +BackgroundTrans, %CKey7%
Gui 5: Add, Hotkey, vCKey8 x95 y180 +BackgroundTrans, %CKey8%
Gui 5: Add, Hotkey, vCKey9 x95 y205 +BackgroundTrans, %CKey9%
Gui 5: Add, Hotkey, vCKey10 x95 y230 +BackgroundTrans, %CKey10%
Gui 5: Add, Hotkey, vCKey11 x95 y255 +BackgroundTrans, %CKey11%
Gui 5: Add, Hotkey, vCKey12 x95 y280 +BackgroundTrans, %CKey12%
Gui 5: Add, Hotkey, vCKey13 x95 y305 +BackgroundTrans, %CKey13%
Gui 5: Add, Hotkey, vCKey14 x95 y330 +BackgroundTrans, %CKey14%

Gui 5: Add, Edit, vCText1 x295 y5 w250 +BackgroundTrans, %CText1%
Gui 5: Add, Edit, vCText2 x295 y30 w250 +BackgroundTrans, %CText2%
Gui 5: Add, Edit, vCText3 x295 y55 w250 +BackgroundTrans, %CText3%
Gui 5: Add, Edit, vCText4 x295 y80 w250 +BackgroundTrans, %CText4%
Gui 5: Add, Edit, vCText5 x295 y105 w250 +BackgroundTrans, %CText5%
Gui 5: Add, Edit, vCText6 x295 y130 w250 +BackgroundTrans, %CText6%
Gui 5: Add, Edit, vCText7 x295 y155 w250 +BackgroundTrans, %CText7%
Gui 5: Add, Edit, vCText8 x295 y180 w250 +BackgroundTrans, %CText8%
Gui 5: Add, Edit, vCText9 x295 y205 w250 +BackgroundTrans, %CText9%
Gui 5: Add, Edit, vCText10 x295 y230 w250 +BackgroundTrans, %CText10%
Gui 5: Add, Edit, vCText11 x295 y255 w250 +BackgroundTrans, %CText11%
Gui 5: Add, Edit, vCText12 x295 y280 w250 +BackgroundTrans, %CText12%
Gui 5: Add, Edit, vCText13 x295 y305 w250 +BackgroundTrans, %CText13%
Gui 5: Add, Edit, vCText14 x295 y330 w250 +BackgroundTrans, %CText14%

Gui, Show, w1095 h350, Keybinder by puRe
return

Einstellungen:
Gui 2: Show, w1095 h375, Keybinder by puRe @ Einstellungen
return

Hotkeys:
Gui 3: Show, w1095 h375, Keybinder by puRe @ Hotkeys
return

Textbinds:
Gui 4: Show, w1195 h375, Keybinder by puRe @ Textbinds
return

CustomKeys:
Gui 5: Show, w1095 h375, Keybinder by puRe @ Eigene Binds
return

InitKeys()
{
	Loop, 31
	{
		tmp:= Key%A_Index%
		IfNotEqual,tmp, ErrorLevel
		{
			tmp:= "~" tmp
			Hotkey, %tmp%, KeyLabel%A_Index%
		}
	}
}

ExitKeys()
{
	Loop, 31
	{
		IfNotEqual,tmp,
		{
			tmp:= Key%A_Index%
			tmp:= "~" tmp
			Hotkey, %tmp%, Off
		}
	}
}

InitCKeys()
{
	Loop, 14
	{
		tmp:= CKey%A_Index%
		IfNotEqual,tmp, ErrorLevel
		{
			tmp:= "~" tmp
			Hotkey, %tmp%, CKeyLabel%A_Index%
		}
	}
}

ExitCKeys()
{
	Loop, 14
	{
		IfNotEqual,tmp,
		{
			tmp:= CKey%A_Index%
			tmp:= "~" tmp
			Hotkey, %tmp%, Off
		}
	}
}

Timer1:
If(WinExist("GTA:SA:MP"))
{
	if(WinActive("GTA:SA:MP"))
	{
		if(DLLLoaded == 0)
		{
			Sleep, 3000
			Load_Gametext()
			DllLoaded := 1
			UpdateDataEx()
			UpdateScoreboardData()
			GetPlayerName(myName, true)
		}
		Loop, Read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
		{
			LastLine:= A_LoopReadLine
			if(Instr(LastLine,"] SERVER: Willkommen"))
			{
				ChatMsg("Der Keybinder wurde gestartet, willkommen " . myName . ".")
				break
			}
			else If(Instr(LastLine, "Sie sind schneller als"))
			{
				RegExMatch(LastLine, "\*BLITZER\* Sie sind schneller als (.*)\((.*)\) km\/h gefahren. Strafe: \$(.*), (.*) Punkte \(100\$ pro km\/h, Blitzer: (.*)\)",Blitzer)
				ChatMsg("Du wurdest geblitzt, achte doch auf die Umwelt!")
				break
			}
			else if(InStr(LastLine, ", Sender: "))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] (.*), Sender: (.*) \((.*)\)", SMS)
				GetPlayerName(myName, true)
				if(!InStr(SMS5, myName))
				{
					LastNumber := SMS6
					SaveAll()
					break
				}
				break
			}
			else if(Instr(LastLine, "hat dich aus dem Gefängnis geholt."))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] Anwalt (.*) hat dich aus dem Gefängnis geholt.", Anwalt)
				ChatMsg("Der Anwalt " . Anwalt4 . " hat sich aus dem Gefängnis befreit.")
				break
			}
			else if(Instr(LastLine, "überwiesen bekommen"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\]    Sie haben \$(.*) von (.*) überwiesen bekommen. Grund: (.*)" ,Uberwiesen)
				ChatMsg("Du hast von " Uberwiesen5 ", $" Uberwiesen4 " überwiesen bekommen. Grund: " Uberwiesen6)
				break
			}
			else if(InStr(LastLine, "Du hast deine Schuld gegenüber der Gesellschaft beglichen."))
			{
				break
			}
			else if (Instr(LastLine, "Wanted gelöscht. Grund:"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Officer (.*) hat dir 1 Wanted gelöscht. Grund: (.*)", ClearA)
				SendChat(ClearA4 " hat mir 1 Wanted gelöscht, Grund: " ClearA5)
				Wanted_Level -= 1
				SaveAll()
				break
			}
			else if (Instr(LastLine, "Wanteds gelöscht. Grund:"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Officer (.*) hat dir (.*) Wanteds gelöscht. Grund: (.*)", ClearB)
				SendChat(ClearB4 " hat mir " ClearB5 " Wanted gelöscht, Grund: " ClearB6)
				Wanted_Level -= ClearB5
				SaveAll()
				break
			}
			else if(InStr(LastLine,"hat dein Beschusswanted gelöscht. Grund:"))
			{
				RegExMatch(LastLine,"i)\[(.*):(.*):(.*)\] \* Officer (.*) hat dein Beschusswanted gelöscht. Grund: (.*)", wclear)
				Wanted_Level-=2
				SaveAll()
				break
			}
			else if(Instr(LastLine,"hat sich in deine Polizeiakte gehackt"))
			{
				RegExMatch(LastLine,"i)\[(.*):(.*):(.*)\] (.*) hat sich in deine Polizeiakte gehackt und (.*) Wanteds entfernt.", hacker)
				Wanted_Level -= hacker5
				ChatMsg("Dir wurden soeben {FF0000}" hacker5 "{FFFFFF} Wanteds gehackt von " hacker4)
				SaveAll()
				break
			}
			else if (Instr(LastLine, "Sekunden eingesperrt.   Kaution: nicht möglich"))
			{
				RegExMatch(LastLine, "Du wurdest von (.*) für (.*) Sekunden eingesperrt.   Kaution: nicht möglich",Arrest)
				ChatMsg("Eingesperrt für {FF0000}" number_format(Arrest2) "{FFFFFF} Sekunden ( {FF0000}" . FormatSeconds(Arrest2) . "{FFFFFF} )")
				Arrest2 := StrReplace(Arrest2, ".", "")
				dailyPrison += Arrest2
				monthlyPrison += Arrest2
				globalPrison += Arrest2
				Wanted_Level 		:= 0
				Wanted_Grund 		:= "Unknown"
				Wanted_Reporter 	:= "Unknown"
				Wanted_Time 		:= "00:00:00"
				SaveAll()
				break
			}
			else if (Instr(LastLine, "SERVER: Es ist jetzt 0:00 Uhr"))
			{
				RegExMatch(LastLine, "SERVER: Es ist jetzt (.*):(.*) Uhr",Zeit)
				NewDayStats()
				break
			}
			else if(InStr(LastLine, "] * Du hast 2 Gramm Drogen genommen!"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Du hast 2 Gramm Drogen genommen!", drugsused)
				DrogenDabei -= 2
				DrogenGenommen += 2
				ChatMsg("Du hast noch {FF0000}" DrogenDabei " {FFFFFF}Drogen bei dir und schon {FF0000}" DrogenGenommen "g{FFFFFF} genommen!")
				dailyDrugs += 2
				monthlyDrugs += 2
				globalDrugs += 2
				SaveAll()
				SetTimer, DrogenTimer, off
				SetTimer, DrogenTimer, 1000
				break
			}
			else if(InStr(LastLine, "] * Du warst in der Entzugsklinik und bist jetzt nicht mehr stoned!"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Du warst in der Entzugsklinik und bist jetzt nicht mehr stoned!", drogenentzug)
				DrogenGenommen := 0
				SaveAll()
				ChatMsg("Der Drogen-Zähler wurde zurückgesetzt.")
				break
			}
			else if(InStr(LastLine, "Sekunden eingesperrt und verlierst wegen Flucht und Kill durch einen Polizist"))
			{
				Regexmatch(LastLine, "\* Du wurdest für (.*) Sekunden eingesperrt und verlierst wegen Flucht und Kill durch einen Polizist \$(.*) .", var)
				ChatMsg("Du wurdest wegen Flucht für {FF0000}" FormatSeconds(var1) "{FFFFFF} eingesperrt und verlierst {FF0000}$" number_format(var2))
				var1 := StrReplace(var1, ".", "")
				dailyPrison += var1
				monthlyPrison += var1
				globalPrison += var1
				Wanted_Level 		:= 0
				Wanted_Grund 		:= "Unknown"
				Wanted_Reporter 	:= "Unknown"
				Wanted_Time 		:= "00:00:00"
				Death(0)
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				break
			}
			else if(InStr(LastLine, "Du hast folgendes Verbrechen begangen:"))
			{
				RegExMatch(LastLine, "Du hast folgendes Verbrechen begangen: \[(.*)\], Zeuge: (.*)", Mord)
				if(Mord1 = "Mord")
				{
					FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
					Kill(0)
				}
				Wanted_Grund 		:= Mord1
				Wanted_Reporter 	:= Mord2
				Wanted_Time 		:= A_Hour . ":" . A_Min . ":" . A_Sec
			}
			else if (Instr(LastLine, "Du hast wegen deines Todes"))
			{
				RegExMatch(LastLine, "Du hast wegen deines Todes \$(.*) aus der Geldbörse verloren!" ,var)
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				Death(0)
				break
			}
			else If InStr(LastLine,"] * Aktuelles Wantedlevel:")
			{
				RegExMatch(LastLine,"\* Aktuelles Wantedlevel: (.*)" ,wantedx)
				ChatMsg("Neues Wantedlevel: " wantedx1)
				Wanted_Level := wantedx1
				SaveAll()
				break
			}
			else if(Instr(LastLine, "Drogen aus der SafeBox genommen!"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\]   (.*) Drogen aus der SafeBox genommen\!", Safeboxgenommen)
				Safeboxgenommen4 := StrReplace(Safeboxgenommen4, ".", "")
				DrogenDabei += Safeboxgenommen4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "Drogen in der SafeBox hinterlegt!"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\]   (.*) Drogen in der SafeBox hinterlegt\!", Safeboxhinter)
				Safeboxhinter4 := StrReplace(Safeboxhinter4, ".", "")
				DrogenDabei -= Safeboxhinter4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(Instr(LastLine, "Drogen aus der Gang-SafeBox (neuer Stand: "))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\]    (.*)g Drogen aus der Gang-SafeBox \(neuer Stand: (.*)g\) genommen\!", GSafeboxgenommen)
				GSafeboxgenommen4 := StrReplace(GSafeboxgenommen4, ".", "")
				DrogenDabei += GSafeboxgenommen4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "Drogen (nur Gang-Drogen) in der Gang-SafeBox (neuer Stand: "))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\]    (.*)g Drogen \(nur Gang-Drogen\) in der Gang-SafeBox \(neuer Stand: (.*)g\) hinterlegt\!", GSafeboxhinter)
				GSafeboxhinter4 := StrReplace(GSafeboxhinter4, ".", "")
				DrogenDabei -= GSafeboxhinter4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			
			else if(InStr(LastLine, "] * Du hast") && InStr(LastLine, "g Drogen in den Kofferraum gelegt."))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Du hast (\d+)g Drogen in den Kofferraum gelegt\.", kofferraumgelegtedrugs)
				kofferraumgelegtedrugs4 := StrReplace(kofferraumgelegtedrugs4, ".", "")
				DrogenDabei -= kofferraumgelegtedrugs4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "] * Du hast") && InStr(LastLine, "g Drogen aus dem Kofferraum genommen."))
			{
				RegExMatch(A_LoopReadLine, "i)\[(.*):(.*):(.*)\] \* Du hast (\d+)g Drogen aus dem Kofferraum genommen\.", kofferraumgenommenedrugs)
				kofferraumgenommenedrugs4 := StrReplace(kofferraumgenommenedrugs4, ".", "")
				DrogenDabei += kofferraumgenommenedrugs4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "] * Du hast") && InStr(LastLine, "für") && InStr(LastLine, "von Drogendealer") && InStr(LastLine, "gekauft."))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] \* Du hast (.*) Gramm für \$(.*) von Drogendealer (.*) gekauft.", drogenbuy)
				drogenbuy4 := StrReplace(drogenbuy4, ".", "")
				DrogenDabei += drogenbuy4
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "hat dir") && InStr(LastLine,"Gramm abgekauft, die Summe von"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] (.*) hat dir (.*) Gramm abgekauft, die Summe von \$(.*) wird zu deinem PayCheck addiert\.", drogenverkaufen)
				drogenverkaufen5 := StrReplace(drogenverkaufen5, ".", "")
				DrogenDabei -= drogenverkaufen5
				ChatMsg("Du hast jetzt {FF0000}" DrogenDabei "g{FFFFFF} Drogen bei dir")
				SaveAll()
				break
			}
			else if(InStr(LastLine, "/accept"))
			{
				RegExMatch(LastLine, "/accept (\w*)", tmpService)
				Service := tmpService1
				break
			}
			else if(InStr(LastLine,"Du hast dir") && InStr(LastLine,"Munition für"))
			{
				RegExMatch(LastLine, "i)\[(.*):(.*):(.*)\] Du hast dir (.*) mit (.*) Munition für \$(.*) \((.*)\)",waffen)
				waffen6 := StrReplace(waffen6, ".", "")
				dailyWaffen += waffen6
				monthlyWaffen += waffen6
				globalWaffen += waffen6
				SaveAll()
				break
			}
			else if(InStr(LastLine, "Zinsen:"))
			{
				RegExMatch(LastLine, "i)Zinsen: \{00FF00\}\+\$(.*) \{B4B5B7\}\((.*)\)", zins)
				if(zins1 == "" || zins1 = "ERROR")
				{
					break
				}
				dailyPayday += 1
				monthlyPayday += 1
				globalPayday += 1
				ChatMsg("Du hast beim Payday {FF0000}" number_format(zins1) "{FFFFFF} erhalten")
				zins1 := StrReplace(zins1, ".", "")
				dailyZinsen += zins1
				monthlyZinsen += zins1
				globalZinsen += zins1
				ChatMsg("Zinsen heute: {FF0000}$" number_format(dailyZinsen) "{FFFFFF} | Monat: {FF0000}$" number_format(monthlyZinsen) "{FFFFFF} | Gesamt: {FF0000}$" number_format(globalZinsen))
				SaveAll()
				break
			}
		}
		FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\chatlog.txt
		
		
		Loop, Read, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
		{
			letze_zeile:=A_LoopReadLine
			If(InStr(letze_zeile,"~g~gangwarkill"))
			{
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				Kill(0)
				break
			}
			else if(InStr(letze_zeile, "~r~gangwarkill"))
			{
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				Death(0)
				break
			}
			else if(InStr(letze_zeile,"~g~gangzonekill"))
			{
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				Kill(1)
				break
			}
			else if(InStr(letze_zeile, "~r~gangzonekill"))
			{
				FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
				Death(1)
				break
			}
			else if(InStr(letze_zeile,"~n~~w~Restliche Haftstrafe:"))
			{
				RegExMatch(letze_zeile, "Haftstrafe: (\w*)", zeit)
				StringTrimLeft, zeit, zeit, 12
				ChatMsg("Restliche Haftstrafe: " . FormatSeconds(zeit1) . ".")
				break
			}
		}
		FileDelete, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
	}
}
else {
	DllLoaded := 0
}
return

FischenTimer:
IniRead, MinLBS, Settings.ini, Settings, MinLBS
Sleep 100
ChatMsg("Du fängst nun an Fische ab {FF0000}" MinLBS "{FFFFFF} LBS zu fangen.")
sleep 500
loop,
{
	if(bFischen == false)
		return
	SendChat("/fish")
	Sleep 100
	GetChatLine(0,fished)
	Sleep 150
	if(InStr(fished, "Du hast einen"))
	{
		RegExMatch(fished, "\* Du hast einen (.*) gefangen, Gewicht (.*) Lbs.", fishinfo)
		if(fishinfo2 >= MinLBS && fishes != 5) 
		{
			SendChat("/me packt einen " fishinfo1 " mit " fishinfo2 " LBS in die Kühlbox!")
			if(wSound == 1)
				soundbeep
			fishes++
		}
		else 
		{
			SendChat("/throwback")
		}
	}
	else if(InStr(fished, "in keinem der Fischgebiete")) {
		bFischen := false
		SetTimer, FischenTimer, off
		ChatMsg("{FFFFFF}Fahre zu einem geeigneten Angelplatz um zu angeln mit /fischen!")
		return
	}
	else if(InStr(fished, "Du hast bereits 5 Fische")) {
		if(fishes == 5)
		{
			bFischen := false
			ChatMsg("Du hast bereits {FF0000}5{FFFFFF} Fische gefangen. Das Angeln wurde {DF0101}beendet{FFFFFF}!")
			settimer, FischenTimer, off
			return
		}
		else if(fishes != 5) {
			bFischen := false
			ChatMsg("Du hast bereits {FF0000}5{FFFFFF} Fische gefangen. Das Angeln wurde {FF0000}beendet{FFFFFF}!")
			settimer, FischenTimer, off
			return
		}
	}
	else if(InStr(fished, "Zu viele Fische gefangen, warte eine Weile"))
	{
		if(fishes != 5) 
		{
			ChatMsg("Warte einen Augenblick, das Angeln geht gleich weiter!")
			sleep 200
			SetTimer, FischenTimer, off
			sleep 100
			Settimer, FischenTimer, 10000
			return
		}
		else if(fishes == 5)
		{
			bFischen := false
			ChatMsg("Warte einen Augenblick, das Angeln geht gleich weiter!")
			sleep 200
			settimer, FischenTimer, off
		}
	}
	else if(InStr(fished, "Du bist nicht am richtigen Ort"))
	{
		bFischen := false
		SetTimer, FischenTimer, off
		ChatMsg("Fahre zu einem geeigneten Angelplatz um zu angeln mit /fischen!")
		return
	}
	sleep 1200
}
return

SaveAll() {
	IniWrite, %DrogenDabei%, %A_WorkingDir%\Settings.ini, Settings, DrogenDabei
	IniWrite, %DrogenGenommen%, %A_WorkingDir%\Settings.ini, Settings, DrogenGenommen
	
	IniWrite, %LastNumber%, %A_WorkingDir%\Settings.ini, Settings, LastSMS
	
	IniWrite, %Wanted_Level%, %A_WorkingDir%\Settings.ini, Settings, WantedLevel
	IniWrite, %Wanted_Reporter%, %A_WorkingDir%\Settings.ini, Settings, WantedReporter
	IniWrite, %Wanted_Grund%, %A_WorkingDir%\Settings.ini, Settings, WantedGrund
	IniWrite, %Wanted_Time%, %A_WorkingDir%\Settings.ini, Settings, WantedTime
	
	IniWrite, %globalKills%, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
	IniWrite, %dailyKills%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Kills
	IniWrite, %monthlyKills%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Kills
	
	IniWrite, %globalDeaths%, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
	IniWrite, %dailyDeaths%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Tode
	IniWrite, %monthlyDeaths%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Tode
	
	IniWrite, %globalZinsen%, %A_WorkingDir%\Statistiken\Global.ini, Global, Zinsen
	IniWrite, %dailyZinsen%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Zinsen
	IniWrite, %monthlyZinsen%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Zinsen
	
	IniWrite, %globalPayday%, %A_WorkingDir%\Statistiken\Global.ini, Global, Payday
	IniWrite, %dailyPayday%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Payday
	IniWrite, %monthlyPayday%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Payday
	
	IniWrite, %globalWaffen%, %A_WorkingDir%\Statistiken\Global.ini, Global, Waffen
	IniWrite, %dailyWaffen%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Waffen
	IniWrite, %monthlyWaffen%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Waffen
	
	IniWrite, %globalPrison%, %A_WorkingDir%\Statistiken\Global.ini, Global, Prison
	IniWrite, %dailyPrison%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Prison
	IniWrite, %monthlyPrison%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Prison
	
	IniWrite, %globalDrugs%, %A_WorkingDir%\Statistiken\Global.ini, Global, Drugs
	IniWrite, %dailyDrugs%, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Drugs
	IniWrite, %monthlyDrugs%, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Drugs
}

Kill(type) {
	FileAppend, FERTIG, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
	IniRead, KillSpruch, %A_WorkingDir%\Settings.ini, Settings, KillSpruch
	IniRead, GangZoneSpruch, %A_WorkingDir%\Settings.ini, Settings, GangZoneSpruch

	IniRead, KillStreaks, %A_WorkingDir%\Settings.ini, Settings, KillStreaks
	IniRead, KillSpruche, %A_WorkingDir%\Settings.ini, Settings, KillSpruche
	IniRead, KillSpruche1, %A_WorkingDir%\Settings.ini, Settings, KillSpruche1
	IniRead, KillSpruche2, %A_WorkingDir%\Settings.ini, Settings, KillSpruche2
	IniRead, KillSpruche3, %A_WorkingDir%\Settings.ini, Settings, KillSpruche3
	IniRead, KillSpruche4, %A_WorkingDir%\Settings.ini, Settings, KillSpruche4
	IniRead, KillSpruche5, %A_WorkingDir%\Settings.ini, Settings, KillSpruche5

	globalKills+=1
	dailyKills+=1
	monthlyKills+=1
	V3 := dailyKills - dailyDeaths
	V4 := Round(dailyKills/dailyDeaths,3)
	GetPlayerZone(MyZone)
	if(GetPlayerInterior())
		MyZone := "einem Interior"
	; %KILLS% - killAnzahl 
	; %ZONE% - MyZone
	; %DKILLS% - killsHeute
	; 
	KillSpruch := RegExReplace(KillSpruch, "%ZONE%", MyZone)
	KillSpruch := RegExReplace(KillSpruch, "%KILLS%", globalKills)
	KillSpruch := RegExReplace(KillSpruch, "%DKILLS%", dailyKills)
	
	GangZoneSpruch := RegExReplace(GangZoneSpruch, "%ZONE%", MyZone)
	GangZoneSpruch := RegExReplace(GangZoneSpruch, "%KILLS%", globalKills)
	GangZoneSpruch := RegExReplace(GangZoneSpruch, "%DKILLS%", dailyKills)
	
	StringSplit, KillSpruch, KillSpruch, \
	StringSplit, GangZoneSpruch, GangZoneSpruch, \
	
	if(type == 1) {
		loop, %GangZoneSpruch0% {
				SendChat(GangZoneSpruch%A_Index%)
		}
	}
	else {
		loop, %KillSpruch0% {
			SendChat(KillSpruch%A_Index%)
		}
	}

	currentKills++
	if(KillStreaks == 1) {
		if(Mod(currentKills, 3) == 0) {
			SendChat("/f Ich habe einen " currentKills "er KillStreak.")
		}
	}
	if(KillSpruche == 1) {
		Random, rnd, 1, 5
		SendChat(KillSpruche%rnd%)
	}

	ChatMsg("Heute {FF0000}" dailyKills "{FFFFFF} Kills | Tode {FF0000}" dailyDeaths "{FFFFFF} | Dif {FF0000}" V3 "{FFFFFF} | DKD {FF0000}" V4)
	SaveAll()
}

Death(type) {
	FileAppend, FERTIG, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
	globalDeaths+=1
	dailyDeaths+=1
	monthlyDeaths+=1
	
	V3 := dailyKills - dailyDeaths
	V4 := Round(dailyKills/dailyDeaths,3)
	GetPlayerZone(MyZone)
	if(GetPlayerInterior())
		MyZone := "einem Interior"
	if(type == 1)
		SendChat("/f Ich bin soeben in " MyZone " (Gebiet) gestorben")
	else
		SendChat("/f Ich bin soeben in " MyZone " gestorben")
	ChatMsg("Heute {FF0000}" dailyKills "{FFFFFF} Kills | Tode {FF0000}" dailyDeaths "{FFFFFF} | Dif {FF0000}" V3 "{FFFFFF} | DKD {FF0000}" V4)
	currentKills := 0
	DrogenDabei := 0
	DrogenGenommen := 0
	SaveAll()
}

DrogenTimer:
Sleep, 19000
health := GetPlayerHealth()
health := round(health, 0)
ChatMsg("Drogen einnehmen wieder möglich! (HP: {FF0000}" . health . "{FFFFFF})")
SetTimer, DrogenTimer, off
return

GuiClose:
SaveAll()
ExitApp
return

2GuiClose:
Gui, Submit, NoHide
IniWrite, %DrugsMitnehmen%, %A_WorkingDir%\Settings.ini, Settings, DrugsMitnehmen
IniWrite, %DeagleKaufen%, %A_WorkingDir%\Settings.ini, Settings, DeagleKaufen
IniWrite, %ShotgunKaufen%, %A_WorkingDir%\Settings.ini, Settings, ShotgunKaufen

IniWrite, %WeaponPack1%, %A_WorkingDir%\Settings.ini, Settings, WeaponPack1
IniWrite, %WeaponPack2%, %A_WorkingDir%\Settings.ini, Settings, WeaponPack2
IniWrite, %WeaponPack3%, %A_WorkingDir%\Settings.ini, Settings, WeaponPack3
IniWrite, %WeaponPack4%, %A_WorkingDir%\Settings.ini, Settings, WeaponPack4
IniWrite, %WeaponPackAmmo1%, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo1
IniWrite, %WeaponPackAmmo2%, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo2
IniWrite, %WeaponPackAmmo3%, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo3
IniWrite, %WeaponPackAmmo4%, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo4

IniWrite, %Spruch1%, %A_WorkingDir%\Settings.ini, Settings, KillSpruch
IniWrite, %Spruch2%, %A_WorkingDir%\Settings.ini, Settings, GangZoneSpruch
IniWrite, %ChatNachricht%, %A_WorkingDir%\Settings.ini, Settings, ChatNachricht
IniWrite, %KillStreaks%, %A_WorkingDir%\Settings.ini, Settings, KillStreaks
IniWrite, %KillSpruche%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche
IniWrite, %KillSpruche1%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche1
IniWrite, %KillSpruche2%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche2
IniWrite, %KillSpruche3%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche3
IniWrite, %KillSpruche4%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche4
IniWrite, %KillSpruche5%, %A_WorkingDir%\Settings.ini, Settings, KillSpruche5
Gui, 2: Hide
return

3GuiClose:
ExitKeys()
SaveKeys()
InitKeys()
TrayTip, Keybinder by puRe, Hotkeys wurden gespeichert, 3
Gui, 3: Hide
return

5GuiClose:
ExitCKeys()
SaveCKeys()
SaveCTexts()
InitCKeys()
TrayTip, Keybinder by puRe, Eigebe Binds wurden gespeichert, 3
Gui, 5: Hide
return

SaveKeys() {
	Gui, Submit, NoHide
	Loop, 31
	{
		tmp:= Key%A_Index%
		IniWrite,%tmp%,Keys.ini,Keys,Key%A_Index%
	}
}

SaveCKeys() {
	Gui, Submit, NoHide
	Loop, 14
	{
		tmp:= CKey%A_Index%
		IniWrite,%tmp%,CKeys.ini,CKeys,CKey%A_Index%
	}
}

SaveCTexts() {
	Gui, Submit, NoHide
	Loop, 14
	{
		tmp:= CText%A_Index%
		IniWrite,%tmp%,CKeys.ini,CKeys,CText%A_Index%
	}
}
return

;~ #################################################### KeyLabels
#include keylabels.ahk
#include custom_keylabels.ahk
;~ ######################################################################################## TextBinds
#Include textbinds.ahk
;~ ######################################################################Funktionen
#Include functions.ahk