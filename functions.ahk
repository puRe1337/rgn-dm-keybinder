ChatMsg(msg)
{
	global ChatNachricht
	AddChatMessage(0xFF0000, ChatNachricht . " {FFFFFF}" . msg)
}

countMembers(Find, Find2="")
{
	counter := 0
	loop {
		GetChatLine(A_Index - 1, chat)
		if(InStr(chat, Find) || InStr(chat, Find2))
			counter := A_Index
		else
			break
	}
	return counter
}

getTime()
{
	FormatTime, Uhrzeit, Format
	return %Uhrzeit%
}

;FileLog(file, what)
{
	FileAppend, %what%, %file%
	FileAppend, `n, %file%
}

number_format(num)
{
	return RegExReplace(num, "\B(?=(\d{3})+(?!\d))", ".")
}

NewDayStats() {
	global
	FormatTime,Today,,dd.MM.yyyy
	FormatTime,Month,,MMMM yyyy
	IfNotExist, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini
	{
		dailyKills 			:= 0
		dailyDeaths 		:= 0
		dailyDrugs 			:= 0
		dailyZinsen 		:= 0
		dailyPayday 		:= 0
		dailyPrison 		:= 0
		dailyWaffen			:= 0
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Kills
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Tode
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Drugs
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Zinsen
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Payday
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Prison
		IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Waffen
	}
	IfNotExist, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini
	{
		monthlyKills 		:= 0
		monthlyDeaths		:= 0
		monthlyDrugs 		:= 0
		monthlyZinsen 		:= 0
		monthlyPayday 		:= 0
		monthlyPrison 		:= 0
		monthlyWaffen		:= 0
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Kills
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Tode
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Drugs
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Zinsen
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Payday
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Prison
		IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Waffen
	}
	ChatMsg("Eine neue Tagesstatistik wurde angelegt, falls nicht starte den Keybinder neu")
}

FormatSeconds(Sekunden)
{
	Return SubStr("0" . Sekunden // 3600, -1) . ":"
		. SubStr("0" . Mod(Sekunden, 3600) // 60, -1) . ":"
		. SubStr("0" . Mod(Sekunden, 60), -1)
}

GetDialogLine(Line, ByRef Output){
	chatindex := 0
	FileRead, file, %A_MyDocuments%\GTA San Andreas User Files\SAMP\gametexts.txt
	loop, Parse, file, `n, `r
		{
			if(A_LoopField)
			chatindex := A_Index
		}
		loop, Parse, file, `n, `r
		{
			if(A_Index = chatindex - line){
				output := A_LoopField
				break
			}
		}
	return
}

PlayerInput(text){
	s := A_IsSuspended
	Suspend On
	KeyWait Enter
	;~ BlockChatInput() ;Auskommentieren, falls die API nicht genutzt wird
	SendInput t^a{backspace}%text%
	Input, var, v, {enter}
	SendInput ^a{backspace 100}{enter}
	Sleep, 20
	;~ UnblockChatInput() ;Auskommentieren, falls die API nicht genutzt wird
	if(!s)
		Suspend Off
	return var
}