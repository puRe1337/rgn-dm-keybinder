﻿#NoEnv

PATH_API := RelToAbs(A_ScriptDir, "bin/puReAPI.dll")

hModule := DllCall("LoadLibrary", Str, PATH_API)
if(hModule == -1 || hModule == 0)
{
	MsgBox, 48, Error, puReAPI.dll wurde nicht gefunden
	ExitApp
}

/*

//Player.h
EXPORT int		API_GetPlayerPointer		();
EXPORT int		API_GetPlayerHealth			();
EXPORT int		API_GetPlayerArmor			();
EXPORT int		API_GetPlayerInterior		();
EXPORT int		API_GetPlayerPosition		( float &fPosX, float &fPosY, float &fPosZ, float &fPosR );
EXPORT int		API_GetPlayerZone			( char* &szZoneName );
EXPORT int		API_GetPlayerCity			( char* &szCityName );
EXPORT int		API_IsPlayerInRange			( float fPosX, float fPosY, float fPosZ, float fRadius );
EXPORT int		API_IsPlayerInRange2D		( float fPosX, float fPosY, float fRadius );

//Vehicle.h
EXPORT int		API_GetVehiclePointer		();
EXPORT float	API_GetVehicleHealth		();
EXPORT int		API_GetVehicleModelID		();
EXPORT int		API_GetVehicleType			();
EXPORT int		API_IsPlayerDriver			();
EXPORT int		API_GetVehicleLockState		();
EXPORT int		API_GetVehicleEngineState	();
EXPORT int		API_GetVehicleLightState	();
EXPORT float	API_GetVehicleSpeed			( float fMult );
EXPORT int		API_GetVehicleModelName		( int iModelID, char* &szModelName );
EXPORT int		API_GetVehiclePosition		( float &fPosX, float &fPosY, float &fPosZ );

//Weapon.h
EXPORT int API_GetPlayerWeaponClip			(int iSlot);
EXPORT int API_GetPlayerTotalWeaponClip		(int iSlot);
EXPORT int API_GetPlayerWeaponID			(int iSlot);
EXPORT int API_GetPlayerWeaponSlot			();
EXPORT int API_GetPlayerWeaponName			(int iSlot, char* &szWeaponName);

//Interface.h
EXPORT int API_SetWantedLevelColor			(int iColor);
EXPORT int API_SetMoneyColor				(int iColor);
EXPORT int API_SetHealthBarColor			(int iColor);

//SAMP.h
EXPORT int API_AddChatMessage					(DWORD dwColor, char* szText);
EXPORT int API_ShowGameText						(char* szText, int iTime, int iStyle);
EXPORT int API_SendChat							(char* szText);
EXPORT int API_PlayAudioStream					(char* szLink);
EXPORT int API_StopAudioStream					();
EXPORT int API_ShowDialog						(int iStyle, char* szCaption, char* szInfo, char* szButton);
EXPORT int API_BlockChatInput					(bool bBlock);
EXPORT int API_IsChatOpen						();
EXPORT int API_IsDialogOpen						();
EXPORT int API_GetChatLine						(int iLine, char* &szText);

EXPORT int API_GetPlayerIdByName				(char* szName, bool bRefresh);
EXPORT int API_GetPlayerIdByNameIgnoreCase		(char* szName, bool bRefresh);
EXPORT int API_GetPlayerPingById				(int iID, bool bRefresh);
EXPORT int API_GetPlayerScoreById				(int iID, bool bRefresh);
EXPORT int API_GetPlayerNameById				(int iID, bool bRefresh, char* &szName);
EXPORT int API_UpdateScoreboardData				();
EXPORT int API_UpdateDataEx						();

EXPORT int API_GetPlayerName					(char* &szName, bool bRefresh);
EXPORT int API_GetPlayerId						(bool bRefresh);
EXPORT int API_GetPlayerScore					(bool bRefresh);
EXPORT int API_GetPlayerPing					(bool bRefresh);

*/


;~ //Player.h
GetPlayerPointer_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerPointer")
GetPlayerHealth_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerHealth")
GetPlayerArmor_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerArmor")
GetPlayerInterior_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerInterior")	
GetPlayerPosition_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerPosition")
GetPlayerZone_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerZone")
GetPlayerCity_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerCity")
IsPlayerInRange_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_IsPlayerInRange")
IsPlayerInRange2D_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_IsPlayerInRange2D")

;~ //Vehicle.h
GetVehiclePointer_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehiclePointer")	
GetVehicleHealth_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleHealth")
GetVehicleModelID_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleModelID")
GetVehicleType_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleType")
IsPlayerDriver_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_IsPlayerDriver")	
GetVehicleLockState_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleLockState")	
GetVehicleEngineState_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleEngineState")	
GetVehicleLightState_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleLightState")
GetVehicleSpeed_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleSpeed")
GetVehicleModelName_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehicleModelName")
GetVehiclePosition_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetVehiclePosition")

;~ //Weapon.h
GetPlayerWeaponClip_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerWeaponClip")
GetPlayerTotalWeaponClip_func				:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerTotalWeaponClip")
GetPlayerWeaponID_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerWeaponID")
GetPlayerWeaponSlot_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerWeaponSlot")
GetPlayerWeaponName_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerWeaponName")

;~ //Interface.h
SetWantedLevelColor_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_SetWantedLevelColor")
SetMoneyColor_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_SetMoneyColor")
SetHealthBarColor_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_SetHealthBarColor")

;~ //SAMP.h
AddChatMessage_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_AddChatMessage")
ShowGameText_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_ShowGameText")
SendChat_func								:= DllCall("GetProcAddress", UInt, hModule, Str, "API_SendChat")
PlayAudioStream_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_PlayAudioStream")
StopAudioStream_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_StopAudioStream")
ShowDialog_func								:= DllCall("GetProcAddress", UInt, hModule, Str, "API_ShowDialog")
BlockChatInput_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_BlockChatInput")
IsChatOpen_func								:= DllCall("GetProcAddress", UInt, hModule, Str, "API_IsChatOpen")
IsDialogOpen_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_IsDialogOpen")
GetChatLine_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetChatLine")

GetPlayerIdByName_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerIdByName")			
GetPlayerIdByNameIgnoreCase_func			:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerIdByNameIgnoreCase")
GetPlayerPingById_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerPingById")
GetPlayerScoreById_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerScoreById")
GetPlayerNameById_func						:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerNameById")
UpdateScoreboardData_func					:= DllCall("GetProcAddress", UInt, hModule, Str, "API_UpdateScoreboardData")
UpdateDataEx_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_UpdateDataEx")

GetPlayerName_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerName")
GetPlayerId_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerId")
GetPlayerScore_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerScore")
GetPlayerPing_func							:= DllCall("GetProcAddress", UInt, hModule, Str, "API_GetPlayerPing")





;~ //Player.h
GetPlayerPointer() {
	global GetPlayerPointer_func
	res := DllCall(GetPlayerPointer_func)
	return res
}

GetPlayerHealth() {
	global GetPlayerHealth_func
	res := DllCall(GetPlayerHealth_func, "Float")
	return res
}

GetPlayerArmor() {
	global GetPlayerArmor_func
	res := DllCall(GetPlayerArmor_func, "Float")
	return res
}

GetPlayerInterior() {
	global GetPlayerInterior_func
	res := DllCall(GetPlayerInterior_func)
	return res
}

GetPlayerPosition(ByRef fPosX, ByRef fPosY, ByRef fPosZ, ByRef fPosR) {
	global GetPlayerPosition_func
	res := DllCall(GetPlayerPosition_func, FloatP, fPosX, FloatP, fPosY, FloatP, fPosZ, FloatP, fPosR)
	return res
}

GetPlayerZone(ByRef szZoneName) {
	global GetPlayerZone_func
	VarSetCapacity(szZoneName, 32)
	res := DllCall(GetPlayerZone_func, StrP, szZoneName)
	return res
}

GetPlayerCity(ByRef szCityName) {
	global GetPlayerCity_func
	VarSetCapacity(szCityName, 32)
	res := DllCall(GetPlayerCity_func, StrP, szCityName)
	return res
}

IsPlayerInRange(fPosX, fPosY, fPosZ, fRadius) {
	global IsPlayerInRange_func
	res := DllCall(IsPlayerInRange_func, Float, fPosX, float, fPosY, float, fPosZ, float, fRadius)
	return res
}

IsPlayerInRange2D(fPosX, fPosY, fRadius) {
	global IsPlayerInRange2D_func
	res := DllCall(IsPlayerInRange2D_func, Float, fPosX, float, fPosY, float, fRadius)
	return res
}



;~ //Vehicle.h
GetVehiclePointer() {
	global GetVehiclePointer_func
	res := DllCall(GetVehiclePointer_func)
	return res
}

GetVehicleHealth() {
	global GetVehicleHealth_func
	res := DllCall(GetVehicleHealth_func, "Float")
	return res
}

GetVehicleModelID() {
	global GetVehicleModelID_func
	res := DllCall(GetVehicleModelID_func)
	return res
}

GetVehicleType() {
	global GetVehicleType_func
	res := DllCall(GetVehicleType_func)
	return res
}

IsPlayerDriver() {
	global IsPlayerDriver_func
	res := DllCall(IsPlayerDriver_func)
	return res
}

GetVehicleLockState() {
	global GetVehicleLockState_func
	res := DllCall(GetVehicleLockState_func)
	return res
}

GetVehicleEngineState() {
	global GetVehicleEngineState_func
	res := DllCall(GetVehicleEngineState_func)
	return res
}

GetVehicleLightState() {
	global GetVehicleLightState_func
	res := DllCall(GetVehicleLightState_func)
	return res
}

GetVehicleSpeed(fMult) {
	global GetVehicleSpeed_func
	res := DllCall(GetVehicleSpeed_func, Float, fMult, "Float")
	return res
}

GetVehicleModelName(iModelID, ByRef szModelName) {
	global GetVehicleModelName_func
	VarSetCapacity(szModelName, 50)
	res := DllCall(GetVehicleModelName_func, Int, iModelID, StrP, szModelName)
	return res
}

GetVehiclePosition(ByRef fPosX, ByRef fPosY, ByRef fPosZ) {
	global GetVehiclePosition_func
	res := DllCall(GetVehiclePosition_func, FloatP, fPosX, FloatP, fPosY, FloatP, fPosZ)
	return res
}




;~ //Weapon.h
GetPlayerWeaponClip(iSlot) {
	global GetPlayerWeaponClip_func
	res := DllCall(GetPlayerWeaponClip_func, Int, iSlot)
	return res
}

GetPlayerTotalWeaponClip(iSlot) {
	global GetPlayerTotalWeaponClip_func
	res := DllCall(GetPlayerTotalWeaponClip_func, Int, iSlot)
	return res
}

GetPlayerWeaponID(iSlot) {
	global GetPlayerWeaponID_func
	res := DllCall(GetPlayerWeaponID_func, Int, iSlot)
	return res
}

GetPlayerWeaponSlot() {
	global GetPlayerWeaponSlot_func
	res := DllCall(GetPlayerWeaponSlot_func)
	return res
}

GetPlayerWeaponName(iSlot, ByRef szWeaponName) {
	global GetPlayerWeaponName_func
	VarSetCapacity(szWeaponName, 50)
	res := DllCall(GetPlayerWeaponName_func, Int, iSlot, StrP, szWeaponName)
	return res
}



;~ //Interface.h
SetHealthBarColor(iColor) {
	global SetHealthBarColor_func
	res := DllCall(SetHealthBarColor_func, Int, iColor)
	return res
}

SetMoneyColor(iColor) {
	global SetMoneyColor_func
	res := DllCall(SetMoneyColor_func, Int, iColor)
	return res
}

SetWantedLevelColor(iColor) {
	global SetWantedLevelColor_func
	res := DllCall(SetWantedLevelColor_func, Int, iColor)
	return res
}

;~ //SAMP.h


AddChatMessage(iColor, szText) {
	global AddChatMessage_func
	res := DllCall(AddChatMessage_func, Int, iColor, Str, szText)
	return res
}

ShowGameText(szText, iTime, iStyle) {
	global ShowGameText_func
	res := DllCall(ShowGameText_func, Str, szText, Int, iTime, Int, iStyle)
	return res
}

SendChat(szText) {
	global SendChat_func
	res := DllCall(SendChat_func, Str, szText)
	return res
}

PlayAudioStream(szText) {
	global PlayAudioStream_func
	res := DllCall(PlayAudioStream_func, Str, szText)
	return res
}

StopAudioStream() {
	global StopAudioStream_func
	res := DllCall(StopAudioStream_func)
	return res
}

ShowDialog(iStyle, szCaption, szInfo, szButton) {
	global ShowDialog_func
	res := DllCall(ShowDialog_func, Int, iStyle, Str, szCaption, Str, szInfo, Str, szButton)
	return res
}

BlockChatInput(bBlock) {
	global BlockChatInput_func
	res := DllCall(BlockChatInput_func, UChar, bBlock)
	return res
}

IsChatOpen() {
	global IsChatOpen_func
	res := DllCall(IsChatOpen_func)
	return res
}

IsDialogOpen() {
	global IsDialogOpen_func
	res := DllCall(IsDialogOpen_func)
	return res
}

GetChatLine(iLine, ByRef szText) {
	global GetChatLine_func
	VarSetCapacity(szText, 256)
	res := DllCall(GetChatLine_func, Int, iLine, StrP, szText)
	return res
}

GetPlayerIdByName(szName, bRefresh) {
	global GetPlayerIdByName_func
	res := DllCall(GetChatLine_func, Str, szName, UChar, bRefresh)
	return res
}

GetPlayerIdByNameIgnoreCase(szName, bRefresh) {
	global GetPlayerIdByNameIgnoreCase_func
	res := DllCall(GetPlayerIdByNameIgnoreCase_func, Str, szName, UChar, bRefresh)
	return res
}

GetPlayerPingById(iID, bRefresh) {
	global GetPlayerPingById_func
	res := DllCall(GetPlayerPingById_func, Int, iID, UChar, bRefresh)
	return res
}

GetPlayerScoreById(iID, bRefresh) {
	global GetPlayerScoreById_func
	res := DllCall(GetPlayerScoreById_func, Int, iID, UChar, bRefresh)
	return res
}

GetPlayerNameById(iID, bRefresh, ByRef szName) {
	global GetPlayerNameById_func
	VarSetCapacity(szName, 32)
	res := DllCall(GetPlayerNameById_func, Int, iID, UChar, bRefresh, StrP, szName)
	return res
}

UpdateScoreboardData() {
	global UpdateScoreboardData_func
	res := DllCall(UpdateScoreboardData_func)
	return res
}

UpdateDataEx() {
	global UpdateDataEx_func
	res := DllCall(UpdateDataEx_func)
	return res
}

GetPlayerName(ByRef szName, bRefresh) {
	global GetPlayerName_func
	VarSetCapacity(szName, 32)
	res := DllCall(GetPlayerName_func, StrP, szName, UChar, bRefresh)
	return res
}

GetPlayerId(bRefresh) {
	global GetPlayerId_func
	res := DllCall(GetPlayerId_func, UChar, bRefresh)
	return res
}

GetPlayerScore(bRefresh) {
	global GetPlayerScore_func
	res := DllCall(GetPlayerScore_func, UChar, bRefresh)
	return res
}

GetPlayerPing(bRefresh) {
	global GetPlayerPing_func
	res := DllCall(GetPlayerPing_func, UChar, bRefresh)
	return res
}

RelToAbs(root, dir, s = "\") {
	pr := SubStr(root, 1, len := InStr(root, s, "", InStr(root, s . s) + 2) - 1)
		, root := SubStr(root, len + 1), sk := 0
	If InStr(root, s, "", 0) = StrLen(root)
		StringTrimRight, root, root, 1
	If InStr(dir, s, "", 0) = StrLen(dir)
		StringTrimRight, dir, dir, 1
	Loop, Parse, dir, %s%
	{
		If A_LoopField = ..
			StringLeft, root, root, InStr(root, s, "", 0) - 1
		Else If A_LoopField =
			root =
		Else If A_LoopField != .
			Continue
		StringReplace, dir, dir, %A_LoopField%%s%
	}
	Return, pr . root . s . dir
}