﻿IfNotExist, %A_WorkingDir%\Statistiken
{
	FileCreateDir, %A_WorkingDir%\Statistiken
}
IfNotExist, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini
{
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Kills
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Tode
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Drugs
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Zinsen
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Payday
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Prison
	IniWrite, 0, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Waffen
}
IfNotExist, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini
{
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Kills
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Tode
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Drugs
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Zinsen
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Payday
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Prison
	IniWrite, 0, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Waffen
}
IfNotExist, %A_WorkingDir%\Statistiken\Global.ini
{
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Drugs
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Zinsen
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Payday
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Prison
	IniWrite, 0, %A_WorkingDir%\Statistiken\Global.ini, Global, Waffen
}
IfNotExist, %A_WorkingDir%\Settings.ini
{
	IniWrite, /f +1 GangWarKill in `%ZONE`% - Kill Nr. `%KILLS`%, %A_WorkingDir%\Settings.ini, Settings, KillSpruch
	IniWrite, /f +1 G A N G Z O N E in `%ZONE`% - Kill Nr. `%KILLS`%, %A_WorkingDir%\Settings.ini, Settings, GangZoneSpruch
	IniWrite, "D O M E: ", %A_WorkingDir%\Settings.ini, Settings, ChatNachricht
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, LastSMS
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, KidnapPartner
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, KidnapOpfer
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, DrogenDabei
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, DrogenGenommen
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WantedLevel
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WantedReporter
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WantedGrund
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WantedTime
	IniWrite, 12, %A_WorkingDir%\Settings.ini, Settings, DrugsMitnehmen
	IniWrite, 150, %A_WorkingDir%\Settings.ini, Settings, DeagleKaufen
	IniWrite, 100, %A_WorkingDir%\Settings.ini, Settings, ShotgunKaufen
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPack1
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPack2
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPack3
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPack4
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo1
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo2
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo3
	IniWrite, 0, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo4
	IniWrite, 150, %A_WorkingDir%\Settings.ini, Settings, MinLBS
}

IniRead, dailyKills, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Kills
IniRead, dailyDeaths, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Tode
IniRead, dailyDrugs, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Drugs
IniRead, dailyZinsen, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Zinsen
IniRead, dailyPayday, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Payday
IniRead, dailyPrison, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Prison
IniRead, dailyWaffen, %A_WorkingDir%\Statistiken\Tagesstatistik vom %Today%.ini, %Today%, Waffen

IniRead, monthlyKills, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Kills
IniRead, monthlyDeaths, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Tode
IniRead, monthlyDrugs, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Drugs
IniRead, monthlyZinsen, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Zinsen
IniRead, monthlyPayday, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Payday
IniRead, monthlyPrison, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Prison
IniRead, monthlyWaffen, %A_WorkingDir%\Statistiken\Monatsstatistik vom %Month%.ini, %Month%, Waffen

IniRead, globalKills, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
IniRead, globalDeaths, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
IniRead, globalDrugs, %A_WorkingDir%\Statistiken\Global.ini, Global, Drugs
IniRead, globalZinsen, %A_WorkingDir%\Statistiken\Global.ini, Global, Zinsen
IniRead, globalPayday, %A_WorkingDir%\Statistiken\Global.ini, Global, Payday
IniRead, globalPrison, %A_WorkingDir%\Statistiken\Global.ini, Global, Prison
IniRead, globalWaffen, %A_WorkingDir%\Statistiken\Global.ini, Global, Waffen

IniRead, Wanted_Level, %A_WorkingDir%\Settings.ini, Settings, WantedLevel
IniRead, Wanted_Reporter, %A_WorkingDir%\Settings.ini, Settings, WantedReporter
IniRead, Wanted_Grund, %A_WorkingDir%\Settings.ini, Settings, WantedGrund
IniRead, Wanted_Time, %A_WorkingDir%\Settings.ini, Settings, WantedTime

IniRead, LastNumber, %A_WorkingDir%\Settings.ini, Settings, LastSMS

IniRead, DrugsMitnehmen, %A_WorkingDir%\Settings.ini, Settings, DrugsMitnehmen
IniRead, DeagleKaufen, %A_WorkingDir%\Settings.ini, Settings, DeagleKaufen
IniRead, ShotgunKaufen, %A_WorkingDir%\Settings.ini, Settings, ShotgunKaufen

IniRead, WeaponPack1, %A_WorkingDir%\Settings.ini, Settings, WeaponPack1
IniRead, WeaponPack2, %A_WorkingDir%\Settings.ini, Settings, WeaponPack2
IniRead, WeaponPack3, %A_WorkingDir%\Settings.ini, Settings, WeaponPack3
IniRead, WeaponPack4, %A_WorkingDir%\Settings.ini, Settings, WeaponPack4
IniRead, WeaponPackAmmo1, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo1
IniRead, WeaponPackAmmo2, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo2
IniRead, WeaponPackAmmo3, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo3
IniRead, WeaponPackAmmo4, %A_WorkingDir%\Settings.ini, Settings, WeaponPackAmmo4
IniRead, KillSpruch, %A_WorkingDir%\Settings.ini, Settings, KillSpruch
IniRead, GangZoneSpruch, %A_WorkingDir%\Settings.ini, Settings, GangZoneSpruch
IniRead, ChatNachricht, %A_WorkingDir%\Settings.ini, Settings, ChatNachricht
IniRead, KillStreaks, %A_WorkingDir%\Settings.ini, Settings, KillStreaks
IniRead, KillSpruche, %A_WorkingDir%\Settings.ini, Settings, KillSpruche
IniRead, KillSpruche1, %A_WorkingDir%\Settings.ini, Settings, KillSpruche1
IniRead, KillSpruche2, %A_WorkingDir%\Settings.ini, Settings, KillSpruche2
IniRead, KillSpruche3, %A_WorkingDir%\Settings.ini, Settings, KillSpruche3
IniRead, KillSpruche4, %A_WorkingDir%\Settings.ini, Settings, KillSpruche4
IniRead, KillSpruche5, %A_WorkingDir%\Settings.ini, Settings, KillSpruche5

Loop, 31
{
	tmp:= Key%A_Index%
	IniRead, tmp, Keys.ini, Keys, Key%A_Index%, ErrorLevel
	Key%A_Index%:= tmp
}
Loop, 14
{
	tmp:= CKey%A_Index%
	IniRead, tmp, CKeys.ini, CKeys, CKey%A_Index%, ErrorLevel
	CKey%A_Index%:= tmp
	
	tmp := CText%A_Index%
	IniRead, tmp, CKeys.ini, CKeys, CText%A_Index%, ErrorLevel
	CText%A_Index% := tmp
}