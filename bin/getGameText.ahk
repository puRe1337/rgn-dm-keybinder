﻿#NoEnv

hModule := DllCall("LoadLibrary", Str, A_ScriptDir . "\bin\getGameText.dll")
if(hModule == -1 || hModule == 0)
{
	MsgBox, 48, miniAPI - Fehler,Die getGameText.dll wurde nicht gefunden, der Keybinder wird beendet.
	ExitApp
}

Load_func := DllCall("GetProcAddress", UInt, hModule, Str, "Load")
Load_Gametext()
{
	global Load_func
	Result := DllCall(Load_func)
	return Result
}

;~ oder DllCall("getGameText03Z.dll\Load")