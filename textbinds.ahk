:?:/deagle:: ;deagle kaufen
Suspend Permit
SendInput /buygun deagle{space}
return

:?:/shotgun:: ;shotgun kaufen
Suspend Permit
SendInput /buygun shotgun{space}
return

:?:/mp5:: ;mp5 kaufen
Suspend Permit
SendInput /buygun mp5{space}
return

:?:/m4:: ;m4 kaufen
Suspend Permit
SendInput /buygun m4{space}
return

:?:/rifle:: ;rifle kaufen
Suspend Permit
SendInput /buygun rifle{space}
return

:?:/sniper:: ;sniper kaufen
Suspend Permit
SendInput /buygun sniper{space}
return

:?:/pa:: ;partner für nap system
Suspend Permit
SendInput /id{space}
input, Partner_Var, V I M, {enter}
Sleep 300
GetChatLine(0, Line)
if(InStr(Line, "ID:"))
{
	RegExMatch(Line, "ID\: \((.*)\) (.*), Level (.*)",pa)
	Sendchat("/f Ich bin nun mit " . pa2 . " unterwegs")
	IniWrite, %pa2%,  %A_WorkingDir%\Settings.ini, Settings, KidnapPartner
}
return

:?:/ku:: ;opfer für nap systen
Suspend Permit
SendInput /id{space}
input, Partner_Var, V I M, {enter}
Sleep 300
GetChatLine(0, Line)
if(InStr(Line, "ID:"))
{
	RegExMatch(Line, "ID\: \((.*)\) (.*), Level (.*)",pa)
	Sendchat("/f Ich habe " . pa2 . " als Kidnapp 'Ziel' eingetragen")
	IniWrite, %pa2%,  %A_WorkingDir%\Settings.ini, Settings, KidnapOpfer
}
return

:?:/napinfo:: ;napinfo für nap system
Suspend Permit
IniRead, Partner, %A_WorkingDir%\Settings.ini, Settings, KidnapPartner, 0
IniRead, Opfer, %A_WorkingDir%\Settings.ini, Settings, KidnapOpfer, 0
ChatMsg("-->NapInfo<--")
ChatMsg("Partner: " Partner)
ChatMsg("Opfer: " Opfer)
ChatMsg(Key11 " = Hinten Links , " Key12 " =  Hinten Rechts")
return

:?:/setkills:: ;Kills setzen
Suspend Permit
kills := PlayerInput("Neue Kills: ")
ChatMsg("Kills geändert auf: {FF0000}" kills)
IniWrite, %kills%, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
globalKills := kills
return

:?:/settode:: ;Tode setzen
Suspend Permit
tode := PlayerInput("Neue Tode: ")
ChatMsg("Tode geändert auf: {FF0000}" tode)
IniWrite, %tode%, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
globalDeaths := tode
return

:?:/addkill:: ;Kill dazu
Suspend Permit
globalKills += 1
IniWrite, %globalKills%, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
ChatMsg("Ein Kill addiert: {FF0000}" globalKills)
return

:?:/addtod:: ;Tod dazu
Suspend Permit
globalDeaths += 1
IniWrite, %globalDeaths%, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
ChatMsg("Ein Tod addiert: {FF0000}" globalDeaths)
return

:?:/clearkill:: ;Kill löschen
Suspend Permit
globalKills -= 1
IniWrite, %globalKills%, %A_WorkingDir%\Statistiken\Global.ini, Global, Kills
ChatMsg("Ein Kill gelöscht: {FF0000}" globalKills)
return

:?:/cleartod:: ;Tod löschen
Suspend Permit
globalDeaths -= 1
IniWrite, %globalDeaths%, %A_WorkingDir%\Statistiken\Global.ini, Global, Tode
ChatMsg("Ein Tod gelöscht: {FF0000}" globalDeaths)
return

:?:/dd:: ;Drogenzähler zurück setzen
Suspend Permit
DrogenDabei := 0
DrogenGenommen := 0
SaveAll()
ChatMsg("Drogenzähler {FF0000}erfolgreich{FFFFFF} zurückgesetzt!")
return

;~ Daily KD
:?:/fdkd:: ;DKD im Fchat
Suspend Permit
DKD := Round(dailyKills/dailyDeaths,3)
DDif := dailyKills-dailyDeaths
if(dailyDeaths==0)
	DKD := Round(dailyKills/1,3)
SendChat("/f Tages Kills " dailyKills " | Tages Tode " dailyDeaths " | Dif " DDif " Kills | DKD " DKD)
return

:?:/bdkd:: ;DKD im /b
Suspend Permit
DKD := Round(dailyKills/dailyDeaths,3)
DDif := dailyKills-dailyDeaths
if(dailyDeaths==0)
	DKD := Round(dailyKills/1,3)
SendChat("Tages Kills " dailyKills " | Tages Tode " dailyDeaths " | Dif " DDif " Kills | DKD " DKD)
return

:?:/dkd:: ;DKD im normalen Chat
Suspend Permit
DKD := Round(dailyKills/dailyDeaths,3)
DDif := dailyKills-dailyDeaths
if(dailyDeaths==0)
	DKD := Round(dailyKills/1,3)
ChatMsg("Tages Kills {FF0000}" dailyKills "{FFFFFF} | Tages Tode {FF0000}" dailyDeaths "{FFFFFF} | Dif {FF0000}" DDif "{FFFFFF} Kills | DKD {FF0000}" DKD)
return

:?:/gdkd:: ;DKD im /g
Suspend Permit
DKD := Round(dailyKills/dailyDeaths,3)
DDif := dailyKills-dailyDeaths
if(dailyDeaths==0)
	DKD := Round(dailyKills/1,3)
SendChat("/g Tages Kills " dailyKills " | Tages Tode " dailyDeaths " | Dif " DDif " Kills | DKD " DKD)
return

;~ Allgemeine KD
:?:/kd:: ;KD im normalen Chat
Suspend Permit
Dif := globalKills - globalDeaths
KD := Round(globalKills/globalDeaths,3)
ChatMsg("Kills {FF0000}" globalKills "{FFFFFF} | Tode {FF0000}" globalDeaths "{FFFFFF} | Dif {FF0000}" Dif "{FFFFFF} | KD {FF0000}" KD)
return

:?:/bkd:: ;KD im /b
Suspend Permit
Dif := globalKills - globalDeaths
KD := Round(globalKills/globalDeaths,3)
SendChat("Kills " globalKills " | Tode " globalDeaths " | Dif " Dif " | KD " KD)
return

:?:/fkd:: ;KD im /f
Suspend Permit
Dif := globalKills - globalDeaths
KD := Round(globalKills/globalDeaths,3)
SendChat("/f Kills " globalKills " | Tode " globalDeaths " | Dif " Dif " | KD " KD)
return

:?:/gkd:: ;KD im /g
Suspend Permit
Dif := globalKills - globalDeaths
KD := Round(globalKills/globalDeaths,3)
SendChat("/g Kills " globalKills " | Tode " globalDeaths " | Dif " Dif " | KD " KD)
return

;~ Monats KD
:?:/bmkd:: ;monats kd im normalen Chat
Suspend Permit
Dif := monthlyKills - monthlyDeaths
KD := Round(monthlyKills/monthlyDeaths,3)
SendChat("Monats Kills " monthlyKills " | Monats Tode " monthlyDeaths " | Dif " Dif " | MKD " KD)
return

:?:/mkd:: ;monats kd im normalen Chat
Suspend Permit
Dif := monthlyKills - monthlyDeaths
KD := Round(monthlyKills/monthlyDeaths,3)
ChatMsg("Monats Kills {FF0000}" monthlyKills "{FFFFFF} | Monats Tode {FF0000}" monthlyDeaths "{FFFFFF} | Dif {FF0000}" Dif "{FFFFFF} | MKD {FF0000}" KD)
return

:?:/fmkd:: ;Monats KD im /F
Suspend Permit
Dif := monthlyKills - monthlyDeaths
KD := Round(monthlyKills/monthlyDeaths,3)
SendChat("/f Monats Kills " monthlyKills " | Monats Tode " monthlyDeaths " | Dif " Dif " | MKD "KD)
return

:?:/gmkd:: ;Monats KD im /g
Suspend Permit
Dif := monthlyKills - monthlyDeaths
KD := Round(monthlyKills/monthlyDeaths,3)
SendChat("/g Monats Kills " monthlyKills " | Monats Tode " monthlyDeaths " | Dif " Dif " | Monats KD "KD)
return

:?:/25k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 25000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/99k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 99000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/100k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 100000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/50k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 50000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/75k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 75000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/150k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 150000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/200k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 200000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/300k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 300000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/500k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 500000{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/999k::
Suspend Permit
SendInput {enter}t/atm{enter}
Sleep 150
SendInput {down}{enter}
Sleep 100
SendInput 999999{enter}
Sleep 100
SendInput {esc}
Suspend off
return

:?:/zinsen::
Suspend Permit
ChatMsg("Zinsen heute: {FF0000}$" number_format(dailyZinsen))
ChatMsg("Zinsen im Monat: {FF0000}$" number_format(monthlyZinsen))
ChatMsg("Alle Zinsen: {FF0000}$" number_format(globalZinsen))
return

:?:/kstand:: ;Finanzen im normalen Chat
Suspend Permit
GetPlayerName(myName, true)
SendChat("/finances " myName)
sleep, 100
GetChatline(0, line)
if(InStr(line, "Level:"))
{
	RegExMatch(line, "Level:\[(.*)\] Respekt:\[(.*)\] Geld:\[\$(.*)\] Bank:\[\$(.*)\] Gesamtvermögen:\[\$(.*)\]", money)
	SendChat("Aktuelles Gesamtvermögen:[$" number_format(money5) "] Level:[" money1 " (RP:" money2 ")]")
}
return

:?:/gstand::  ;Finanzen im /g Chat
Suspend Permit
GetPlayerName(myName, true)
SendChat("/finances " myName)
sleep, 100
GetChatline(0, line)
if(InStr(line, "Level:"))
{
	RegExMatch(line, "Level:\[(.*)\] Respekt:\[(.*)\] Geld:\[\$(.*)\] Bank:\[\$(.*)\] Gesamtvermögen:\[\$(.*)\]", money)
	SendChat("/g Aktuelles Gesamtvermögen:[$" number_format(money5) "] Level:[" money1 " (RP:" money2 ")]")
}
return

:?:/fstand::  ;Finanzen im /f Chat
Suspend Permit
GetPlayerName(myName, true)
SendChat("/finances " myName)
sleep, 100
GetChatline(0, line)
if(InStr(line, "Level:"))
{
	RegExMatch(line, "Level:\[(.*)\] Respekt:\[(.*)\] Geld:\[\$(.*)\] Bank:\[\$(.*)\] Gesamtvermögen:\[\$(.*)\]", money)
	SendChat("/f Aktuelles Gesamtvermögen:[$" number_format(money5) "] Level:[" money1 " (RP:" money2 ")]")
}
return

:?:/mms::  ;SMS System schneller SMS schreiben
:?:/idsms::
Suspend permit
SendInput /ID:{space}
Input, SMSNummer, V I M, {space}{enter}{escape}
SendInput {end}+{home}{Del}{Esc}
SendChat("/number " SMSNummer)
Sleep 250
GetChatLine(0, Chatline)
If InStr(Chatline, ", Ph:")
{
	RegExMatch(Chatline, "Name: .*, Ph: (\w*)" , number)
	SendInput, t^a/sms %number1%{space}
}
return

:?:/re::  ;Antwort / reply funktion
Suspend Permit
SendInput, /t %LastNumber%{space}
return

:?:/wi:: ;/warinfo Shortbind
Suspend Permit
SendInput, {enter}
SendChat("/warinfo")
return

:?:/sc::
Suspend Permit
SendInput, {enter}
SendChat("/spawnchange")
return

:?:/td:: ;/gtake drugs shortbind
Suspend Permit
SendInput, /gtake drugs{space}
return

:?:/pd:: ;/put drugs shortbind
Suspend Permit
SendInput, /put drugs{space}
return

:?:/cd::
Suspend Permit
SendInput,{enter}
Suspend Off
Loop 3, 
{
	SendChat("/s » " 4 - A_Index " « ")
	KeyWait, <, D T1
	if !ErrorLevel
	{
		SendChat("/s Countdown abgebrochen")
	}
}
SendChat("/s » GO «")
return

:?:/as::
Suspend Permit
SendInput, {enter}
SendChat("/accept " Service)
return

:?:/wh::
Suspend Permit
SendInput, {enter}
SendChat("/service")
Sleep, 200
SendInput, {down 5}
SendInput, {enter}
return

:?:/ff::
Suspend Permit
SendInput, {enter}
SendInput, t/fastfood{space 2}1{left 2} 
return

:?:/gr::
Suspend Permit
SendInput, {enter}
SendInput, t/giverank{space}
return

:?:/setdrugs::
Suspend Permit
newdrugsdabei := PlayerInput("Drogen dabei: ")
newdrugsgenommen := PlayerInput("Drogen genommen: ")
if(newdrugsdabei is not number) {
	ChatMsg("Bitte verwende Zahlen dafür!")
	return
}
if(newdrugsgenommen is not number) {
	ChatMsg("Bitte verwende Zahlen dafür!")
	return
}
IniWrite, %newdrugsdabei%, %A_WorkingDir%\Settings.ini, Settings, DrogenDabei
IniWrite, %newdrugsgenommen%, %A_WorkingDir%\Settings.ini, Settings, DrogenGenommen
DrogenDabei := newdrugsdabei
DrogenGenommen := newdrugsgenommen
ChatMsg("Okay!: " newdrugsdabei "g Drogen dabei und " newdrugsgenommen "g Drogen genommen")
return

:?:/ud::
Suspend Permit
SendInput, {enter}
SendChat("/usedrugs")
return

:?:/setlbs::
Suspend Permit
minlbs := PlayerInput("Mindest LBS: ")
if(minlbs is not number) {
	ChatMsg("Bitte verwende Zahlen dafür!")
	return
}
IniWrite, %minlbs%, %A_WorkingDir%\Settings.ini, Settings, MinLBS
ChatMsg("Okay!: Fische werden erst ab " minlbs " mitgenommen!")
return

:?:/fischen::
Suspend Permit
SendInput, {enter}
SetTimer, FischenTimer, 10000
bFischen := true
ChatMsg("Du fängst nun an zu fischen!")
return

:?:/stopfischen::
Suspend Permit
SendInput, {enter}
bFischen := false
SetTimer, FischenTimer, off
ChatMsg("Du hörst nun auf zu fischen!")
return

:?:/drugprice::
Suspend Permit
drugsprice := PlayerInput("Drogenpreis pro Gramm: ")
drugstosell := PlayerInput("Drogen zum Verkauf: ")
if(drugsprice is not number) {
	ChatMsg("Bitte verwende Zahlen dafür!")
	return
}
if(drugstosell is not number) {
	ChatMsg("Bitte verwende Zahlen dafür!")
	return
}
finalprice := drugsprice * drugstosell
finalprice := number_format(finalprice)
SendChat(number_format(drugstosell) "g mit dem Preis von $" number_format(drugsprice) "/g kosten: $" finalprice)
return

:?:/mydrugs::
Suspend Permit
SendChat("/drugmarketprice")
Sleep 150
GetChatLine(0, last_chat_line)
if(InStr(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen")) {
	RegExMatch(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen \$(.*) und \$(.*) je Gramm.", priceinfo)
	ReadStats_s(stats_text)
	drugs := keyForEntry(stats_text, "Drogen")
	drugs := StrReplace(drugs, ".", "")
	priceinfo1 := StrReplace(priceinfo1, ".", "")
	priceinfo2 := StrReplace(priceinfo2, ".", "")
	RegExMatch(drugs, "(.*)g \(\+(.*)g\)", Drogen)
	AllDrugs := Drogen1 + Drogen2
	MinPrice := AllDrugs * priceinfo1
	MaxPrice := AllDrugs * priceinfo2
	ChatMsg("Meine {FF0000}" number_format(AllDrugs) "g{FFFFFF} Drogen haben einen Wert von ($" number_format(MinPrice) " || $" number_format(MaxPrice) ").")
}
return

:?:/bmydrugs::
Suspend Permit
SendChat("/drugmarketprice")
Sleep 150
GetChatLine(0, last_chat_line)
if(InStr(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen")) {
	RegExMatch(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen \$(.*) und \$(.*) je Gramm.", priceinfo)
	ReadStats_s(stats_text)
	drugs := keyForEntry(stats_text, "Drogen")
	drugs := StrReplace(drugs, ".", "")
	priceinfo1 := StrReplace(priceinfo1, ".", "")
	priceinfo2 := StrReplace(priceinfo2, ".", "")
	RegExMatch(drugs, "(.*)g \(\+(.*)g\)", Drogen)
	AllDrugs := Drogen1 + Drogen2
	MinPrice := AllDrugs * priceinfo1
	MaxPrice := AllDrugs * priceinfo2
	SendChat("Meine " number_format(AllDrugs) "g Drogen haben einen Wert von ($" number_format(MinPrice) " || $" number_format(MaxPrice) ").")
}
return

:?:/fmydrugs::
Suspend Permit
SendChat("/drugmarketprice")
Sleep 150
GetChatLine(0, last_chat_line)
if(InStr(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen")) {
	RegExMatch(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen \$(.*) und \$(.*) je Gramm.", priceinfo)
	ReadStats_s(stats_text)
	drugs := keyForEntry(stats_text, "Drogen")
	drugs := StrReplace(drugs, ".", "")
	priceinfo1 := StrReplace(priceinfo1, ".", "")
	priceinfo2 := StrReplace(priceinfo2, ".", "")
	RegExMatch(drugs, "(.*)g \(\+(.*)g\)", Drogen)
	AllDrugs := Drogen1 + Drogen2
	MinPrice := AllDrugs * priceinfo1
	MaxPrice := AllDrugs * priceinfo2
	SendChat("/f Meine " number_format(AllDrugs) "g Drogen haben einen Wert von ($" number_format(MinPrice) " || $" number_format(MaxPrice) ").")
}
return

:?:/gmydrugs::
Suspend Permit
SendChat("/drugmarketprice")
Sleep 150
GetChatLine(0, last_chat_line)
if(InStr(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen")) {
	RegExMatch(last_chat_line, "Der Marktpreis für Drogen liegt aktuell zwischen \$(.*) und \$(.*) je Gramm.", priceinfo)
	ReadStats_s(stats_text)
	drugs := keyForEntry(stats_text, "Drogen")
	drugs := StrReplace(drugs, ".", "")
	priceinfo1 := StrReplace(priceinfo1, ".", "")
	priceinfo2 := StrReplace(priceinfo2, ".", "")
	RegExMatch(drugs, "(.*)g \(\+(.*)g\)", Drogen)
	AllDrugs := Drogen1 + Drogen2
	MinPrice := AllDrugs * priceinfo1
	MaxPrice := AllDrugs * priceinfo2
	SendChat("/g Meine " number_format(AllDrugs) "g Drogen haben einen Wert von ($" number_format(MinPrice) " || $" number_format(MaxPrice) ").")
}
return

:?:/gi::  ;Gebietinfo im /f
Suspend Permit
SendChat("/gebietinfo")
Sleep, 200
GetChatLine(6, Chat6)
GetChatLine(5, Chat5)
GetChatLine(4, Chat4)
GetChatLine(3, Chat3)
GetChatLine(2, Chat2)
GetChatLine(1, Chat1)
GetChatLine(0, Chat0)
if(Instr(Chat1, " Restzeit:"))
{
	RegExMatch(Chat1, "Restzeit: (.*) Tage, (.*) Stunden und (.*) Minuten", regzeit)
	RegExMatch(Chat2, "  Kills: (.*)", regkills)
	RegExMatch(Chat3, "  Herausforderer: (.*)", regher)
	RegExMatch(Chat4, "  Kills: (.*)", reg1kills)
	RegExMatch(Chat5, "  Besitzer: (.*)", regbe)
	SendChat("/f -> Gebietinfo von " regbe1 " - " regher1 " <-")
	SendChat("/f " regbe1 ": " reg1kills1 " Kills - " regher1 ": " regkills1 " Kills")
	SendChat("/f Restzeit: " regzeit1 " Tage " regzeit2 " Stunden " regzeit3 " Minuten")
	return
}
return