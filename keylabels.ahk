KeyLabel1: ;Member zählen
if(IsChatOpen())
	return
SendChat("/members")
Sleep, 100
members:=countMembers("Rank:", "Leader")
ChatMsg("Es sind {FF0000}" members "{FFFFFF} Member online.")
return

KeyLabel2: ;Finances selber
if(IsChatOpen())
	return
GetPlayerName(myName, true)
SendChat("/finances " myName)
sleep, 250
GetChatline(0, line)
if(InStr(line, "Level:"))
{
	RegExMatch(line, "Level:\[(.*)\] Respekt:\[(.*)\] Geld:\[\$(.*)\] Bank:\[\$(.*)\] Gesamtvermögen:\[\$(.*)\]", money)
	level := number_format(money1)
	respekt := number_format(money2)
	geld := number_format(money3)
	bank := number_format(money4)
	gesamt := number_format(money5)
	ChatMsg("Aktuelles Gesamtvermögen:[{FF0000}$" number_format(money5) "{FFFFFF}] Level:[{FF0000}" money1 "{FFFFFF} (RP:{FF0000}" money2 "{FFFFFF})]")
}
return

KeyLabel3:  ;Gebietinfo im /f
if(IsChatOpen())
	return
SendChat("/gebietinfo")
Sleep, 200
GetChatLine(6, Chat6)
GetChatLine(5, Chat5)
GetChatLine(4, Chat4)
GetChatLine(3, Chat3)
GetChatLine(2, Chat2)
GetChatLine(1, Chat1)
GetChatLine(0, Chat0)
if(Instr(Chat1, " Restzeit:"))
{
	RegExMatch(Chat1, "Restzeit: (.*) Tage, (.*) Stunden und (.*) Minuten", regzeit)
	RegExMatch(Chat2, "  Kills: (.*)", regkills)
	RegExMatch(Chat3, "  Herausforderer: (.*)", regher)
	RegExMatch(Chat4, "  Kills: (.*)", reg1kills)
	RegExMatch(Chat5, "  Besitzer: (.*)", regbe)
	SendChat("/f -> Gebietinfo von " regbe1 " - " regher1 " <-")
	SendChat("/f " regbe1 ": " reg1kills1 " Kills - " regher1 ": " regkills1 " Kills")
	SendChat("/f Restzeit: " regzeit1 " Tage " regzeit2 " Stunden " regzeit3 " Minuten")
	return
}
return

KeyLabel4:  ;Motor an und /lock
if(IsChatOpen())
	return
SendChat("/lock")
SendChat("/engine")
return

KeyLabel5:  ;/lock
if(IsChatOpen())
	return
SendChat("/lock")
return

KeyLabel6:  ;/usedrugs
if(IsChatOpen())
	return
SendChat("/usedrugs")
return

KeyLabel7: ;/time mit anwalt/knast zeit
if(IsChatOpen())
	return
SendChat("/time")
Sleep, 200
GetChatLine(1, Chatline)
if(InStr(Chatline, "Restzeit deines Anwalts"))
{
	RegExMatch(Chatline, "Restzeit deines Anwalts: (.*) Sekunden", anwalt)
	ChatMsg("Restzeit des Anwalts: " FormatSeconds(anwalt1))
	return
}
return

KeyLabel8: ;/enter oder /exit
if(IsChatOpen())
	return
if(GetPlayerInterior())
	SendChat("/exit")
else
	SendChat("/enter")
return

KeyLabel9: ;meine HP
if(IsChatOpen())
	return
health := GetPlayerHealth()
health := round(health, 0)
vhealth := round(GetVehicleHealth(), 0)
aText := ""
if(GetVehiclePointer() != 0)
	aText := "{FFFFFF} - Aktuelle DL: {FF0000}" vhealth
ChatMsg("Aktuelle HP: {FF0000}" health aText)
return

KeyLabel10: ;/lights
if(IsChatOpen())
	return
SendChat("/lights")
return


KeyLabel11: ;nappen auf 0
if(IsChatOpen())
	return
IniRead, Partner, %A_WorkingDir%\Settings.ini, Settings, KidnapPartner, 0
IniRead, Opfer, %A_WorkingDir%\Settings.ini, Settings, KidnapOpfer, 0
SendChat("/kidnap " Opfer " " Partner " 0")
return

KeyLabel12: ;nappen auf 1
if(IsChatOpen())
	return
IniRead, Partner, %A_WorkingDir%\Settings.ini, Settings, KidnapPartner, 0
IniRead, Opfer, %A_WorkingDir%\Settings.ini, Settings, KidnapOpfer, 0
SendChat("/kidnap " Opfer " " Partner " 1")
return

KeyLabel13: ;drogenzähler
if(IsChatOpen())
	return
IniRead, DrogenGenommen, %A_WorkingDir%\Settings.ini, Settings, DrogenGenommen, 0
IniRead, DrogenDabei, %A_WorkingDir%\Settings.ini, Settings, DrogenDabei, 0
ChatMsg("Du kannst noch {FF0000}" 28 - DrogenGenommen "g {FFFFFF}Drogen nehmen bis du Stoned bist")
ChatMsg("Und du hast noch {FF0000}" DrogenDabei "g {FFFFFF}Drogen dabei")
return

KeyLabel14: ;vielen dank im chat
if(IsChatOpen())
	return
SendChat("Vielen Dank!")
return

KeyLabel15: ;heilung anfragen
if(IsChatOpen())
	return
SendChat("Erbitte um eine Heilung, Herr Doktor.")
return

KeyLabel16:
if(IsChatOpen())
	return
health := GetPlayerHealth()
GetPlayerZone(MyZone)
if(GetPlayerInterior())
	MyZone := "einem Interior"
health:=round(health,0)
SendChat("/f Ich habe noch " health " HP in " MyZone)
return

KeyLabel17:
FormatTime, Zeit, %A_Now%, HH
if(Zeit >= 6 && Zeit <= 11) {
	SendChat("Ich wünsche Ihnen noch einen schönen Morgen.")
	return
} else if(Zeit >= 12 && Zeit <= 17) {
	SendChat("Ich wünsche Ihnen noch einen schönen Tag.")
	return
} else if(Zeit >= 18 && Zeit <= 23) {
	SendChat("Ich wünsche Ihnen noch einen schönen Abend.")
	return
} else if(Zeit >= 0 && Zeit <= 5) {
	SendChat("Ich wünsche Ihnen noch eine schöne Nacht.")
	return
}
return

KeyLabel18:
if(IsChatOpen())
	return
SendChat("/stats")
return

KeyLabel19: ;deagle kaufen
if(IsChatOpen())
	return
SendChat("/buygun deagle " DeagleKaufen)
return

KeyLabel20: ;shotgun kaufen
if(IsChatOpen())
	return
SendChat("/buygun shotgun " ShotgunKaufen)
return

KeyLabel21:
if(IsChatOpen())
	return
WepNum1:= "deagle"
WepNum2:= "shotgun"
WepNum3:= "mp5"
WepNum4:= "m4"
WepNum5:= "sniper"
WepNum6:= "rifle"
tmp := WepNum%WeaponPack1%
if(WeaponPack1 != "" && WeaponPack1 != "ERROR" && WeaponPackAmmo1 != 0)
	SendChat("/buygun " tmp " " WeaponPackAmmo1)
tmp := WepNum%WeaponPack2%
if(WeaponPack2 != "" && WeaponPack2 != "ERROR" && WeaponPackAmmo2 != 0)
	SendChat("/buygun " tmp " " WeaponPackAmmo2)
tmp := WepNum%WeaponPack3%
if(WeaponPack3 != "" && WeaponPack3 != "ERROR" && WeaponPackAmmo3 != 0)
	SendChat("/buygun " tmp " " WeaponPackAmmo3)
tmp := WepNum%WeaponPack4%
if(WeaponPack4 != "" && WeaponPack4 != "ERROR" && WeaponPackAmmo4 != 0)
	SendChat("/buygun " tmp " " WeaponPackAmmo4)
return

KeyLabel22:
if(IsChatOpen())
	return
SendChat("/carlock")
return

KeyLabel23:
if(IsChatOpen())
	return
SendChat("/fill")
Sleep, 1000
SendChat("/get fuel")
return

KeyLabel24:
if(IsChatOpen())
	return
SendChat("/f Wanteds: [ " Wanted_Level " ] Grund: [ " Wanted_Grund " ]")
Sleep, 100
ChatMsg("Zeit: [ {FF0000}" Wanted_Time "{FFFFFF} ] Zeuge: [ {FF0000}" Wanted_Reporter "{FFFFFF} ]")
return

KeyLabel25:
if(IsChatOpen())
	return
SendChat("/togphone")
return

KeyLabel26:
if(IsChatOpen())
	return
SendChat("/eatpizza")
return

KeyLabel27:
if(IsChatOpen())
	return
SendInput, t{up}{enter}
return

KeyLabel28:
if(IsChatOpen())
	return
SendChat("/gtake drugs " DrugsMitnehmen)
return

KeyLabel29:
if(IsChatOpen())
	return
Kill(0)
return

KeyLabel30:
if(IsChatOpen())
	return
SendChat("/engine")
return

KeyLabel31:
if(IsChatOpen())
	return
SendInput, t/notafk{space}
return